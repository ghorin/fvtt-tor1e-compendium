# FVTT-TOR1e-Compendium

## COMPATIBILITE
- Foundry VTT 11

## DESCRIPTION
Ce Compendium permet de créer :
- Les éléments qui constituent des Personnages (compétences, traits, vertus, récompenses, défauts, capacités spéciales ...)
- L'équipement martial et non-martial
- Les Adversaires
- Les PNJ


Tous ces éléments sont créés à partir d'une macro disponible depuis le Compendium **"TOR1e Compendium FR"**.

**CREDITS**
Plusieurs icones de ce modules sont des créations de <a href="https://game-icons.net/">game-icons.net</a> et sont utilisées sous la licence <a href="https://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.


## UTILISATION
1. Aller dans l'onglet Compendium
2. Cliquer sur le dossier "TOR1e Compendium FR"
3. Dans la fenêetre qui s'ouvre, cliquer sur "Initialiser le compendium"
4. Dans la fenêetre qui s'ouvre, cliquer sur le bouton "Exécuter la macro".
    Déroulement de la macro :
        - Un premier message s'affiche : === TOR1E - FR - COMPENDIUM : DEBUT ===
        - La macro créé les objets et personnages. Cela peut prendre moins de 30 secondes à plusieurs minutes selon la puissance de l'ordinateur.
        - En fin de macro, un second message s'affiche : === TOR1E - FR - COMPENDIUM : FIN ===

