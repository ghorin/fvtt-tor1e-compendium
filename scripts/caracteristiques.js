export class Tor1eCompendiumCaracteristiques {

    async creationCaracteristiques() {
        console.log("===================================================================");
        console.log("          CREATION DES CARACTERISTIQUES DES PERSONNAGES");
        console.log("===================================================================");

        console.log("===== CREER LE DOSSIER DE BASE DES CARACTERISTIQUES====");
        let dossierCaracteristiques = await Folder.create({
            name: 'Caractéristiques',
            type: 'Item',
            sorting: 'a',
            parent: null
        });

        console.log("-------------------------------------------------------------------");
        console.log("          ==> COMPETENCES");
        console.log("-------------------------------------------------------------------");

        console.log("===== CREER LE DOSSIER DE BASE DES COMPETENCES====");
        let dossierCompetences = await Folder.create({
            name: 'Compétences',
            type: 'Item',
            color: '#2e0505',
            sorting: 'm',
            parent: dossierCaracteristiques._id
        });

        console.log("===== CREER LES SOUS-DOSSIERS DES COMPETENCES ====");

        let dossierCompetencesDetaillees = await Folder.create({
            name: "Compétences détaillées",
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierCompetences._id
        });
        let dossierCompetencesGeneriques = await Folder.create({
            name: "Compétences génériques",
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierCompetences._id
        });
        let dossierCompetencesArmes = await Folder.create({
            name: "Compétences d'armes",
            type: 'Item',
            color: '#181b26',
            sorting: 'm',
            parent: dossierCompetences._id
        });

        console.log("===== CREER LES COMPETENCES DETAILLEES ====");

        console.log("... du Corps");
        await Item.create({ name: "Présence", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 85</em></p>"}, favoured: {value: false}, group: {value: 'personality'} } });
        await Item.create({ name: "Athlétisme", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 86</em></p>"}, favoured: {value: false}, group: {value: 'movement'} } });
        await Item.create({ name: "Vigilance", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 87</em></p>"}, favoured: {value: false}, group: {value: 'perception'} } });
        await Item.create({ name: "Exploration", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 88</em></p>"}, favoured: {value: false}, group: {value: 'survival'} } });
        await Item.create({ name: "Chant", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 88</em></p>"}, favoured: {value: false}, group: {value: 'custom'} } });
        await Item.create({ name: "Artisanat", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 89</em></p>"}, favoured: {value: false}, group: {value: 'vocation'} } });
                                
        console.log("... du Coeur");
        await Item.create({ name: "Inspiration", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'heart'}, description: {value: "<p><em>Livre de base, page 86</em></p>"}, favoured: {value: false}, group: {value: 'personality'} } });
        await Item.create({ name: "Voyage", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'heart'}, description: {value: "<p><em>Livre de base, page 86</em></p>"}, favoured: {value: false}, group: {value: 'movement'} } });
        await Item.create({ name: "Intuition", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'heart'}, description: {value: "<p><em>Livre de base, page 87</em></p>"}, favoured: {value: false}, group: {value: 'perception'} } });
        await Item.create({ name: "Guérison", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'heart'}, description: {value: "<p><em>Livre de base, page 88</em></p>"}, favoured: {value: false}, group: {value: 'survival'} } });
        await Item.create({ name: "Courtoisie", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'heart'}, description: {value: "<p><em>Livre de base, page 89</em></p>"}, favoured: {value: false}, group: {value: 'custom'} } });
        await Item.create({ name: "Art de la guerre", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'heart'}, description: {value: "<p><em>Livre de base, page 89</em></p>"}, favoured: {value: false}, group: {value: 'vocation'} } });
                                
        console.log("... de l'Esprit");
        await Item.create({ name: "Persuasion", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'wits'}, description: {value: "<p><em>Livre de base, page 86</em></p>"}, favoured: {value: false}, group: {value: 'personality'} } });
        await Item.create({ name: "Discrétion", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'wits'}, description: {value: "<p><em>Livre de base, page 87</em></p>"}, favoured: {value: false}, group: {value: 'movement'} } });
        await Item.create({ name: "Fouille", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'wits'}, description: {value: "<p><em>Livre de base, page 88</em></p>"}, favoured: {value: false}, group: {value: 'perception'} } });
        await Item.create({ name: "Chasse", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'wits'}, description: {value: "<p><em>Livre de base, page 88</em></p>"}, favoured: {value: false}, group: {value: 'survival'} } });
        await Item.create({ name: "Enigmes", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'wits'}, description: {value: "<p><em>Livre de base, page 89</em></p>"}, favoured: {value: false}, group: {value: 'custom'} } });
        await Item.create({ name: "Connaissances", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesDetaillees._id, data: { associatedStat: {value: 'wits'}, description: {value: "<p><em>Livre de base, page 90</em></p>"}, favoured: {value: false}, group: {value: 'vocation'} } });






        console.log("===== CREER LES COMPETENCES GENERIQUES ====");
        await Item.create({ name: "Personnalité", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesGeneriques._id, data: { associatedStat: {value: 'personality'}, description: {value: "<p><em>Livre de base, page 85</em></p>"}, favoured: {value: false}, group: {value: 'personality'} } });
        await Item.create({ name: "Déplacement", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesGeneriques._id, data: { associatedStat: {value: 'movement'}, description: {value: "<p><em>Livre de base, page 85</em></p>"}, favoured: {value: false}, group: {value: 'movement'} } });
        await Item.create({ name: "Perception", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesGeneriques._id, data: { associatedStat: {value: 'perception'}, description: {value: "<p><em>Livre de base, page 85</em></p>"}, favoured: {value: false}, group: {value: 'perception'} } });
        await Item.create({ name: "Survie", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesGeneriques._id, data: { associatedStat: {value: 'survival'}, description: {value: "<p><em>Livre de base, page 85</em></p>"}, favoured: {value: false}, group: {value: 'survival'} } });
        await Item.create({ name: "Coutume", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesGeneriques._id, data: { associatedStat: {value: 'custom'}, description: {value: "<p><em>Livre de base, page 85</em></p>"}, favoured: {value: false}, group: {value: 'custom'} } });
        await Item.create({ name: "Métier", type: 'skill', img: 'systems/tor1e/assets/images/icons/skill.png', folder : dossierCompetencesGeneriques._id, data: { associatedStat: {value: 'vocation'}, description: {value: "<p><em>Livre de base, page 85</em></p>"}, favoured: {value: false}, group: {value: 'vocation'} } });
                


        console.log("===== CREER LES COMPETENCES D'ARMES ====");
        await Item.create({ name: "Dague", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_dagger.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "Bigot", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_mattock.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "(Epées)", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_swords.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "Epée courte", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_swords.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "Epée", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_swords.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "Epée longue", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_swords.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "(Lances)", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_spears.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "Lance", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_spears.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "Grande lance", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_spears.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "(Haches)", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_axes.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "Hache", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_axes.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "Hache à long manche", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_axes.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "(Arcs)", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_bows.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "Arc", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_bows.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
        await Item.create({ name: "Grand arc", type: 'skill', img: 'systems/tor1e/assets/images/icons/weapon_bows.png', folder : dossierCompetencesArmes._id, permission: {default: 2}, data: { associatedStat: {value: 'body'}, description: {value: "<p><em>Livre de base, page 92</em></p>"}, favoured: {value: false}, group: {value: 'combat'} } });
            






        console.log("-------------------------------------------------------------------");
        console.log("          ==> CAPACITES SPECIALES ");
        console.log("-------------------------------------------------------------------");

        console.log("===== CREER LE DOSSIER DE BASE ====");
        let dossierCapacitesSpeciales = await Folder.create({
            name: 'Capacités spéciales',
            type: 'Item',
            color: '#2e0505',
            sorting: 'm',
            parent: dossierCaracteristiques._id
        });


        console.log("===== CREER LES SOUS-DOSSIERS ====");

        let dossierCapacitesSpecialesNormales = await Folder.create({
            name: '... normales',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierCapacitesSpeciales._id
        });

        let dossierCapacitesSpecialesAmeliorations = await Folder.create({
            name: '... améliorations',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierCapacitesSpeciales._id
        });
        let dossierCapacitesSpecialesPuissantes = await Folder.create({
            name: '... puissantes',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierCapacitesSpeciales._id
        });
        let dossierCapacitesSpecialesDragons = await Folder.create({
            name: '... pour Dragons',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierCapacitesSpeciales._id
        });
        let dossierCapacitesSpecialesMortVivants = await Folder.create({
            name: '... pour Mort-vivants',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierCapacitesSpeciales._id
        });


        console.log("===== CREER LES CAPACITES SPECIALES ====");
        console.log("... normales");
        await Item.create({name: "Abomination", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 240</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Acharnement", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Guide de la Ville du lac, page 23</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Animosité (culture)", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 240</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Assaut brutal", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 240</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Attaque plongeante", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 240</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Autochtone", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Erebor, page 75</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Aversion au soleil", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 240</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Bicéphale", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Les Vestiges du Nord, page 58</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Connaissances régionales (région)", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 95</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Cuir robuste", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 240</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Déconcertant", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 240</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Dresseur", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Ténèbres sur la Forêt noire, page 13</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Dwimmerlaik", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Fondcombe, page 68</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Effroi", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 241</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Enfants innombrables", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Guide des Terres Sauvages, page 120</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Envoûtement", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Ténèbres sur la Forêt noire, page 110</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Epouvantables sortilèges", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 241</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Esprit corbeau", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Fondcombe, page 72</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Etreinte", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 241</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Force dévastatrice", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 288</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Force effroyable", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 241</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Grand bond", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 241</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Grande taille", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 241</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Habitant de la Forêt Noire", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Guide des Terres Sauvages, page 112</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Habitant des ténèbres", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 242</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Horreur du Bois", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Guide des Terres Sauvages, page 119</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Lâcheté", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 242</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Montagnard", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Guide des Terres Sauvages, page 45</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Mot de Puissance et de Terreur", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Fondcombe, page 68</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Nombreux poisons", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Guide des Terres Sauvages, page 100 et 122</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Noyade", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Voir dans le livre.</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Pas de quartier", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 242</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Peur du feu", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 242</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Peur Noire", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Fondcombe, page 68</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Plus noir que les Ténèbres", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Fondcombe, page 68</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Point faible", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Fondcombe, page 68</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Puanteur fétide", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 242</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Résistance abominable", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 242</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Souffle empoisonné", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Guide des Terres Sauvages / Contes et Légendes des Terres Sauvages, page 111 / 141</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Souffle Noir", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Fondcombe, page 68</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Souffrance d’Autrui", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Contes et Légendes des Terres Sauvages, page 127</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Soumission (cible)", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Guide des Terres Sauvages, page 119</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Tentaculaire", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Contes et Légendes des Terres Sauvages, page 20</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Toiles d’illusion", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Guide des Terres Sauvages, page 122</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Trait mortel", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Contes et Légendes des Terres Sauvages, page 56</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Visions de Tourment", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Fondcombe, page 68</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Vitalité surnaturelle", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Contes et Légendes des Terres Sauvages, page 75</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Vitesse du serpent", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 242</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Voix funeste", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Fondcombe, page 68</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Voix impérieuse", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesNormales._id, data: {description : { value: "<p><em>Livre de base, page 242</em></p>" }, active : { value : true}, cost : { value : 1} }});
                        
        console.log("... améliorations");
        await Item.create({name: "Aguerri", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesAmeliorations._id, data: {description : { value: "<p><em>Fondcombe, page 67</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Chevronné", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesAmeliorations._id, data: {description : { value: "<p><em>Fondcombe, page 67</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Enveloppé d'ombres", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesAmeliorations._id, data: {description : { value: "<p><em>Voir dans le livre.</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Expert", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesAmeliorations._id, data: {description : { value: "<p><em>Fondcombe, page 67</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Meurtrier", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesAmeliorations._id, data: {description : { value: "<p><em>Fondcombe, page 67</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Obscur glamour", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesAmeliorations._id, data: {description : { value: "<p><em>Voir dans le livre.</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Obstiné", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesAmeliorations._id, data: {description : { value: "<p><em>Fondcombe, page 67</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Redoutable", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesAmeliorations._id, data: {description : { value: "<p><em>Fondcombe, page 67</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Saisir et noyer", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesAmeliorations._id, data: {description : { value: "<p><em>Voir dans le livre.</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Supérieur", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesAmeliorations._id, data: {description : { value: "<p><em>Fondcombe, page 67</em></p>" }, active : { value : false}, cost : { value : 0} }});
                        
        console.log("... puissantes");
        await Item.create({name: "Coup impitoyable", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesPuissantes._id, data: {description : { value: "<p><em>Fondcombe, page 67</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Haine débridée", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesPuissantes._id, data: {description : { value: "<p><em>Fondcombe, page 67</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Insaisissable", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesPuissantes._id, data: {description : { value: "<p><em>Fondcombe, page 68</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Malice", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesPuissantes._id, data: {description : { value: "<p><em>Fondcombe, page 68</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Souverain", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesPuissantes._id, data: {description : { value: "<p><em>Fondcombe, page 68</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Terrible", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesPuissantes._id, data: {description : { value: "<p><em>Fondcombe, page 68</em></p>" }, active : { value : true}, cost : { value : 1} }});
                                        
        console.log("... pour mort-vivants");
        await Item.create({name: "Fantomatique", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesMortVivants._id, data: {description : { value: "<p><em>Fondcombe, page 76</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Spectral", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesMortVivants._id, data: {description : { value: "<p><em>Fondcombe, page 76</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Vitalité surnaturelle de spectre", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesMortVivants._id, data: {description : { value: "<p><em>Fondcombe, page 76</em></p>" }, active : { value : true}, cost : { value : 1} }});
                                
        console.log("... pour dragons");
        await Item.create({name: "Point faible", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesDragons._id, data: {description : { value: "<p><em>Erebor, page 76</em></p>" }, active : { value : false}, cost : { value : 0} }});
        await Item.create({name: "Souffle ardent", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesDragons._id, data: {description : { value: "<p><em>Erebor, page 76</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Souffle empoisonné", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesDragons._id, data: {description : { value: "<p><em>Erebor, page 76</em></p>" }, active : { value : true}, cost : { value : 1} }});
        await Item.create({name: "Vitesse Féroce", type: 'special-ability', img: 'systems/tor1e/assets/images/icons/adversary_special-ability.png', folder : dossierCapacitesSpecialesDragons._id, data: {description : { value: "<p><em>Erebor, page 76</em></p>" }, active : { value : false}, cost : { value : 0} }});
                








        console.log("-------------------------------------------------------------------");
        console.log("          ==> TRAITS (spécialités et particularités) ");
        console.log("-------------------------------------------------------------------");

        console.log("===== CREER LE DOSSIER DE BASE ====");
        let dossierTraits = await Folder.create({
            name: 'Traits',
            type: 'Item',
            color: '#2e0505',
            sorting: 'a',
            parent: dossierCaracteristiques._id
        });


        console.log("===== CREER LES SOUS-DOSSIERS ====");

        let dossierParticularites = await Folder.create({
            name: 'Particularités',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierTraits._id
        });

        let dossierSpecialites = await Folder.create({
            name: 'Spécialités',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierTraits._id
        });


        console.log("===== CREER LES TRAITS ====");
        console.log("Spécialités");
        await Item.create({ name: "Allume-feu", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 94</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Art équestre", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Les Cavaliers du Rohan, page 142</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Batelier", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 94</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Broderie", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 15</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Cartographie", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Ténèbres sur la Forêt Noire / Guide des Terres Sauvages, page  130 / 84</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Chasse", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 65</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Commandement", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion, page 104</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Commerce", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 94</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Connaissance de la faune", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 94</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Connaissance de la montagne", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Mountainer, page Mountainer</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Connaissance des ennemis [race]", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 94</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Connaissances elfiques", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 95</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Connaissances régionales", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 95</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Connaissance du Gondor", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion, page 65</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Conscience de l-Ombre", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 95</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Conte", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 95</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Cuisine", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 95</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Ennemi de Sauron", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 129</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Folklore", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 95</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Forge", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 96</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Fumer", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 96</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Herboristerie", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 96</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Jardinier", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 96</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Maçonnerie", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 96</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Médecine", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 96</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Ménestrel", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide de la Ville du lac, page 26</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Menuisier", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 96</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Montagnard", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide des Terres Sauvages, page 45</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Monte-en-l-air", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 97</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Natation", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 97</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Noceur", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide des Terres Sauvages, page 30</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Œil de connaisseur", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Bree, page 41</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Pêche", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 97</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Pistage", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 150</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Rimes de savoir", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 97</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Tissage", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide des Terres Sauvages, page 24</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Tradition ancienne", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 97</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Tradition d-Arnor", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 121</em></p>"}, group: {value: 'speciality'} } });
        await Item.create({ name: "Troglodyte", type: 'trait', img: 'systems/tor1e/assets/images/icons/speciality.png', folder : dossierSpecialites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 97</em></p>"}, group: {value: 'speciality'} } });
                
        console.log("Particularités");
        await Item.create({ name: "Amer", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Les Vestiges du Nord, page 95</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Ami des elfes", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Contes et Légendes des Terres Sauvages, page 80</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Astucieux", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 98</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Audacieux", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 98</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Beau", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 98</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Bourru", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 98</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Brutal", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Les Vestiges du Nord, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Cachottier", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 98</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Circonspect", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 98</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Coriace", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 288</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Courageux", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 98</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Courroucé", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 99</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Courroux meurtrier", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 288</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Courtois", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 99</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Cruel", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide des Terres Sauvages, page 30</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Curieux", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 99</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Déterminé", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 99</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Endurci", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 99</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Energique", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 99</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Entêté", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 99</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Excentrique", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide des Terres Sauvages, page 22</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Farouche", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Les Cavaliers du Rohan, page 190</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Fier", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 99</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Force dévastatrice", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 288</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Fou", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide des Terres Sauvages, page 55</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Franc", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 99</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Généreux", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 99</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Gigantesque", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 288</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Grand", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 99</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Honorable", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 99</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Impétueux", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 99</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Inébranlable", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Insaisissable", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Jovial", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Loyal", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Majestueux", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Malin", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Manigances", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Ténèbres sur la Forêt Noire / Guide des Terres Sauvages, page 133 / 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Méfiant", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Miséricordieux", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Observateur", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Ouïe fine", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Passionné", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Patient", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Petit", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Prescient", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 121</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Présomptueux", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Les Vestiges du Nord, page 55</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Preste", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Prudent", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Rancunier", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Les Vestiges du Nord, page 100</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Rapide", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 101</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Résistant", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 101</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Robuste", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 101</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Sévère", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 101</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Sincère", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 101</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Sinistre", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 101</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Téméraire", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 101</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Vengeur", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 101</em></p>"}, group: {value: 'distinctiveFeature'} } });
        await Item.create({ name: "Vue perçante", type: 'trait', img: 'systems/tor1e/assets/images/icons/distinctive_feature.png', folder : dossierParticularites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 101</em></p>"}, group: {value: 'distinctiveFeature'} } });
          



        console.log("-------------------------------------------------------------------");
        console.log("          ==> VERTUS                                               ");
        console.log("-------------------------------------------------------------------");

        console.log("===== CREER LE DOSSIER DE BASE ====");
        let dossierVertus = await Folder.create({
            name: 'Vertus',
            type: 'Item',
            color: '#2e0505',
            sorting: 'a',
            parent: dossierCaracteristiques._id
        });

        console.log("===== CREER LES SOUS-DOSSIERS ====");
        let dossierVertusMaitrises = await Folder.create({
            name: '... Maitrises',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusBardides = await Folder.create({
            name: 'pour Bardides',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusBeornides = await Folder.create({
            name: 'pour Beornides',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusRohirrim = await Folder.create({
            name: 'pour Cavaliers du Rohan',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusHommesBree = await Folder.create({
            name: 'pour Hommes de Bree',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusHommesMinasTirith = await Folder.create({
            name: 'pour Hommes de Minas Tirith',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusHommesPaysDun = await Folder.create({
            name: 'pour Hommes du Pays de Dun',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusHommesDuLac = await Folder.create({
            name: 'pour Hommes du Pays du Lac',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusHommesDesBoisDesTerresSauvages = await Folder.create({
            name: 'pour Hommes des Bois',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusRodeurDuNord = await Folder.create({
            name: 'pour Rôdeurs du Nord',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusHobbitComte = await Folder.create({
            name: 'pour Hobbits de la Comté',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusHobbitBree = await Folder.create({
            name: 'pour Hobbits de Bree',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusHobbitSauvageValleeAnduin = await Folder.create({
            name: "pour Hobbits Sauvage des Vallées de l'Anduin",
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusElfeForetNoire = await Folder.create({
            name: 'pour Elfes de la Forêt Noire',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusElfeCapricieuxForetNoire = await Folder.create({
            name: 'pour Elfes Capricieux de la Forêt Noire',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusElfeLorien = await Folder.create({
            name: 'pour Elfes de la Lorien',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertushautElfeFondcombe = await Folder.create({
            name: 'pour Haut-Elfes de Fondcombe',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusNainErebor = await Folder.create({
            name: 'pour Nain de la Montagne Solitaire',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusNainMontsDeFer = await Folder.create({
            name: 'pour Nain des Monts de Fer',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusNainMontagnesGrises = await Folder.create({
            name: 'pour Nain des Montagnes Grises',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });
        let dossierVertusNainMontagnesBleues = await Folder.create({
            name: 'pour Nain des Montagnes Bleues',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierVertus._id
        });


        console.log("===== CREER LES VERTUS ====");
        console.log("Maitrises");
        await Item.create({ name: "Confiant", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusMaitrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 105</em></p>"}, type: {value: "tor1e.virtues.groups.masteries"} } });
        await Item.create({ name: "Doué", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusMaitrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 105</em></p>"}, type: {value: "tor1e.virtues.groups.masteries"} } });
        await Item.create({ name: "Expert", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusMaitrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 105</em></p>"}, type: {value: "tor1e.virtues.groups.masteries"} } });
        await Item.create({ name: "Main puissante", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusMaitrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 105</em></p>"}, type: {value: "tor1e.virtues.groups.masteries"} } });
        await Item.create({ name: "Main sûre", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusMaitrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 105</em></p>"}, type: {value: "tor1e.virtues.groups.masteries"} } });
        await Item.create({ name: "Résistant", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusMaitrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 105</em></p>"}, type: {value: "tor1e.virtues.groups.masteries"} } });
        await Item.create({ name: "Maitriser la Corruption", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusMaitrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 113</em></p>"}, type: {value: "tor1e.virtues.groups.masteries"} } });
        await Item.create({ name: "Maitriser la Peur", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusMaitrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 113</em></p>"}, type: {value: "tor1e.virtues.groups.masteries"} } });
        await Item.create({ name: "Maitriser sa Protection", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusMaitrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 114</em></p>"}, type: {value: "tor1e.virtues.groups.masteries"} } });
        await Item.create({ name: "Maitriser sa compétence", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusMaitrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 114</em></p>"}, type: {value: "tor1e.virtues.groups.masteries"} } });
        
        console.log("vertus culturelles");
        await Item.create({ name: "Epouvantables visions", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusBardides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 105</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Héritage", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusBardides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 106</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Homme du Roi", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusBardides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 106</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Maître d’Armes", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusBardides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 106</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Tir féroce", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusBardides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 107</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Biscuits au miel", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusBeornides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 107</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Force considérable", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusBeornides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 107</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Frère des ours", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusBeornides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 107</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Peau épaisse", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusBeornides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 108</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Vagabond nocturne", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusBeornides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 108</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Meneurs des Chevaux du Riddermark", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusRohirrim._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 145</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Sinistre bénédiction", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusRohirrim._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 23</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Ecuyer de Maison", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusRohirrim._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 145</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Garde Royale", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusRohirrim._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 146</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Maître du Destin", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusRohirrim._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 146</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Anciennes Chansons et Contes d’Enfants", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusRohirrim._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 146</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Le Courage des Désespérés", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Bree, page 42</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Amical et Indiscret", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Bree, page 42</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "D’après les Souvenirs et les Vieux Contes", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Bree, page 42</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Les Gens de Bree, Petits et Grands", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Bree, page 43</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Heureux qui comme Ulysse", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Bree, page 43</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Capitaine du Gondor", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesMinasTirith._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 67</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Garde de la Tour", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesMinasTirith._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 67</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "A la bataille", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesMinasTirith._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 67</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Par-delà les sentiers périlleux", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesMinasTirith._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 68</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Vue perçante", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesMinasTirith._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 68</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Une peuplade mystérieuse", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesPaysDun._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 152</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Champion des Hommes des Collines", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesPaysDun._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 152</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "D’Allure sinistre", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesPaysDun._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 152</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Mauvais augure", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesPaysDun._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 153</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Assaut sauvage", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesPaysDun._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 153</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Archer de la Guilde", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesDuLac._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide de la Ville du lac, page 28</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Prince Marchand", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesDuLac._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide de la Ville du lac, page 28</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Combat au bouclier", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesDuLac._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide de la Ville du lac, page 28</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Sens des affaires", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesDuLac._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide de la Ville du lac, page 28</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Pied marin", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesDuLac._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide de la Ville du lac, page 29</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Chanson de dévouement", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesDesBoisDesTerresSauvages._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 114</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Chiens de la Forêt Noire", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesDesBoisDesTerresSauvages._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 114</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Herbes médicinales", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesDesBoisDesTerresSauvages._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 115</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "La volonté du chasseur", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesDesBoisDesTerresSauvages._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 115</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Vigilance naturelle", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHommesDesBoisDesTerresSauvages._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 115</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Gardiens intrépides", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusRodeurDuNord._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 124</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Endurance du Rôdeur", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusRodeurDuNord._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 125</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Royauté révélée", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusRodeurDuNord._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 125</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Rumeur de la terre", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusRodeurDuNord._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 125</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Familier de la nature", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusRodeurDuNord._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 125</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Art de la disparition", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitComte._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 112</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Aussi dur que la pierre", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitComte._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 113</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Courageux si besoin", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitComte._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 113</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Droit au but", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitComte._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 113</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Petit peuple", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitComte._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 113</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Le Courage des Désespérés", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Bree, page 42</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Amical et Indiscret", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Bree, page 42</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Les Gens de Bree, Petits et Grands", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Bree, page 43</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Art de la disparition", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 112</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Petit peuple", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 113</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Art de la disparition", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitSauvageValleeAnduin._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 112</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Aussi dur que la pierre", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitSauvageValleeAnduin._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 113</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Courageux si besoin", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitSauvageValleeAnduin._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 113</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Droit au but", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitSauvageValleeAnduin._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 113</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Petit peuple", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitSauvageValleeAnduin._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 113</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Enigmes cinglantes", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusHobbitSauvageValleeAnduin._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 86</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Fléau de l’Ombre", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 110</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Les Hérauts", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 111</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Magie des Elfes Sylvains", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 111</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Mortelle archerie", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 111</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Rêves Elfiques", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 112</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "L’Appel de la Forêt Noire", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeCapricieuxForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 81</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Les Hérauts", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeCapricieuxForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 111</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Magie des Elfes Sylvains", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeCapricieuxForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 111</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Mortelle archerie", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeCapricieuxForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 111</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Rêves Elfiques", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeCapricieuxForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 112</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Pain de route des Elfes", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeLorien._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 50</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Les Hérauts", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeLorien._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 111</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Faveur de la Dame", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeLorien._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 50</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Mortelle archerie", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeLorien._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 111</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Rêves Elfiques", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusElfeLorien._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 112</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Artisan d’Eregion", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertushautElfeFondcombe._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 132</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Beauté des Etoiles", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertushautElfeFondcombe._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 133</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Elbereth Gilthoniel !", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertushautElfeFondcombe._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 133</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Puissance des Premiers-nés", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertushautElfeFondcombe._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 133</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Prouesse des Eldar", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertushautElfeFondcombe._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 133</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Haine ancestrale", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainErebor._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 108</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Nuque roide des Nains", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainErebor._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 108</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Les Corbeaux de la Montagne", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainErebor._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 109</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Sorts brisés", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainErebor._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 110</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Voie de Durin", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainErebor._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 110</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Haine ancestrale", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontsDeFer._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 108</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Nuque roide des Nains", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontsDeFer._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 108</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Serment d’allégeance", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontsDeFer._id, permission: {default: 2}, data: { description: {value: "<p><em>Erebor, page 100</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Sorts brisés", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontsDeFer._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 110</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Voie de Durin", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontsDeFer._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 110</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Haine ancestrale", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontagnesGrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 108</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Nuque roide des Nains", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontagnesGrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 108</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Affaires obscures", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontagnesGrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Erebor, page 105</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Sorts brisés", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontagnesGrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 110</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Voie de Durin", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontagnesGrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 110</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Haine ancestrale", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontagnesBleues._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 108</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Nuque roide des Nains", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontagnesBleues._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 108</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Chants ardents", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontagnesBleues._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 35</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Sorts brisés", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontagnesBleues._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 110</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        await Item.create({ name: "Voie de Durin", type: 'virtues', img: 'systems/tor1e/assets/images/icons/virtue.png', folder : dossierVertusNainMontagnesBleues._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 110</em></p>"}, type: {value: "tor1e.virtues.groups.cultural"} } })
        

        console.log("-------------------------------------------------------------------");
        console.log("          ==> RECOMPENSES                                          ");
        console.log("-------------------------------------------------------------------");

        console.log("===== CREER LE DOSSIER DE BASE ====");
        let dossierRecompenses = await Folder.create({
            name: 'Récompenses',
            type: 'Item',
            color: '#2e0505',
            sorting: 'a',
            parent: dossierCaracteristiques._id
        });

        console.log("===== CREER LES SOUS-DOSSIERS ====");
        let dossierRecompensesQualites = await Folder.create({
            name: '... Qualités',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesBardides = await Folder.create({
            name: 'pour Bardides',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesBeornides = await Folder.create({
            name: 'pour Beornides',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesRohirrim = await Folder.create({
            name: 'pour Cavaliers du Rohan',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesHommesBree = await Folder.create({
            name: 'pour Hommes de Bree',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesHommesMinasTirith = await Folder.create({
            name: 'pour Hommes de Minas Tirith',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesHommesPaysDun = await Folder.create({
            name: 'pour Hommes du Pays de Dun',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesHommesDuLac = await Folder.create({
            name: 'pour Hommes du Lac',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesHommesDesBoisDesTerresSauvages = await Folder.create({
            name: 'pour Hommes des Bois',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesRodeurDuNord = await Folder.create({
            name: 'pour Rôdeurs du Nord',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesHobbitsComte = await Folder.create({
            name: 'pour Hobbits de la Comté',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesHobbitsBree = await Folder.create({
            name: 'pour Hobbits de Bree',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesHobbitSauvageValleeAnduin = await Folder.create({
            name: "pour Hobbits Sauvage des Vallées de l'Anduin",
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesElfeForetNoire = await Folder.create({
            name: 'pour Elfes de la Forêt Noire',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesElfeCapricieuxForetNoire = await Folder.create({
            name: 'pour Elfes Capricieux de la Forêt Noire',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesElfeLorien = await Folder.create({
            name: 'pour Elfes de la Lorien',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompenseshautElfeFondcombe = await Folder.create({
            name: 'pour Haut-Elfes de Fondcombe',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesNainErebor = await Folder.create({
            name: 'pour Nain de la Montagne Solitaire',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesNainMontsDeFer = await Folder.create({
            name: 'pour Nain des Monts de Fer',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesNainMontagnesGrises = await Folder.create({
            name: 'pour Nain des Montagnes Grises',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });
        let dossierRecompensesNainMontagnesBleues = await Folder.create({
            name: 'pour Nain des Montagnes Bleues',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierRecompenses._id
        });



        console.log("===== CREER LES RECOMPENSES ====");
        console.log("Qualités");
        await Item.create({ name: "Astucieux", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesQualites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 118</em></p>"}, type: {value: "tor1e.reward.groups.qualities"} } });
        await Item.create({ name: "Ajusté", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesQualites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 118</em></p>"}, type: {value: "tor1e.reward.groups.qualities"} } });
        await Item.create({ name: "Renforcé", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesQualites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 118</em></p>"}, type: {value: "tor1e.reward.groups.qualities"} } });
        await Item.create({ name: "Dévastateur", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesQualites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 118</em></p>"}, type: {value: "tor1e.reward.groups.qualities"} } });
        await Item.create({ name: "Acéré", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesQualites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 119</em></p>"}, type: {value: "tor1e.reward.groups.qualities"} } });
        await Item.create({ name: "Féroce", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesQualites._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 119</em></p>"}, type: {value: "tor1e.reward.groups.qualities"} } });
       
        console.log("Récompenses culturelles");
        await Item.create({ name: "Arc long de Dale", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesBardides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 118</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Lance du Roi Bladorthin", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesBardides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 118</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Grand pavois", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesBardides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 118</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Lance tueuse de géants", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesBeornides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 118</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Armure noble", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesBeornides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 119</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Hache d’écartèlement", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesBeornides._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 119</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Cotte du Vieux Gondor", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesRohirrim._id, permission: {default: 2}, data: { description: {value: "<p><em>Voir dans le livre.</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Heaume à crinière", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesRohirrim._id, permission: {default: 2}, data: { description: {value: "<p><em>Voir dans le livre.</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Lance étincelante", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesRohirrim._id, permission: {default: 2}, data: { description: {value: "<p><em>Référence, page </em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Lanterne d’Huissier", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre, page Page</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Pipe du Poney Fringant", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 119</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Bouclier des Premiers Hommes", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 119</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Cor de Minas Anor", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesMinasTirith._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 119</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Bouclier de la Citadelle", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesMinasTirith._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 120</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Arme de Grande lignée", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesMinasTirith._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 120</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Armure des Spires", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesPaysDun._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 120</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Cherche-Cœur", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesPaysDun._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 147</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Grande Hache des Forêts", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesPaysDun._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 147</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Armure cuivrée", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesDuLac._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 147</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Pierre d’affutage", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesDuLac._id, permission: {default: 2}, data: { description: {value: "<p><em>Bree, page 43</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Lame-serpent", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesDuLac._id, permission: {default: 2}, data: { description: {value: "<p><em>Bree, page 43</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Hache Barbue", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesDesBoisDesTerresSauvages._id, permission: {default: 2}, data: { description: {value: "<p><em>Bree, page 43</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Armure de Plumes", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesDesBoisDesTerresSauvages._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 68</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Arc du Berger", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHommesDesBoisDesTerresSauvages._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 68</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Relique de l’Arnor", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesRodeurDuNord._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 68</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "L’Etoile des Dúnedain", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesRodeurDuNord._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 153</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Flèches numénoréennes", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesRodeurDuNord._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 153</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Arc des Hauts du Nord", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHobbitsComte._id, permission: {default: 2}, data: { description: {value: "<p><em>Cavaliers du Rohan, page 153</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Lame de Roi", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHobbitsComte._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide de la Ville du Lac, page 29</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Armure de Chance", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHobbitsComte._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide de la Ville du Lac, page 29</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Arc des Hauts du Nord", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHobbitsBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Guide de la Ville du Lac, page 29</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Lame de Roi", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHobbitsBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 122</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Armure de Chance", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHobbitsBree._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 122</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Arc des Marais", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHobbitSauvageValleeAnduin._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 122</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Lame de Roi", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHobbitSauvageValleeAnduin._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 126</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Armure de Chance", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHobbitSauvageValleeAnduin._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 126</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Lance de Pêche", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesHobbitSauvageValleeAnduin._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 122</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Lance amère", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesElfeForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 126</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Bouclier du lancier", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesElfeForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 126</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Arc sylvain", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesElfeForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 126</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Lance amère", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesElfeCapricieuxForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 121</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Bouclier du lancier", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesElfeCapricieuxForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 121</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Arc sylvain", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesElfeCapricieuxForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 122</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Vin des Montagnes Sombres", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesElfeCapricieuxForetNoire._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 121</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Manteau elfique", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesElfeLorien._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 121</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Arc des Galadhrim", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesElfeLorien._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 122</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Broche elfique", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesElfeLorien._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 86</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Anneau mineur", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompenseshautElfeFondcombe._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 121</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Recueils & Cartes du Savoir Interdit", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompenseshautElfeFondcombe._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 121</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Lance de la Dernière Alliance", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompenseshautElfeFondcombe._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 121</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Hache de l’Azanulbizar", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesNainErebor._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 121</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Haubert des Nains", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesNainErebor._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 121</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Heaume d’Effroi", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesNainErebor._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 121</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Hache de l’Azanulbizar", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesNainMontsDeFer._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 81</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Haubert de Pied-d’Acier", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesNainMontsDeFer._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 50</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Heaume d’Effroi", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesNainMontsDeFer._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 50</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Hache de Ver", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesNainMontagnesGrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Adventurer’s Companion (vo), page 50</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Haubert des Nains", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesNainMontagnesGrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 134</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Heaume d’Effroi", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesNainMontagnesGrises._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 134</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "La Harpe des Salles", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesNainMontagnesBleues._id, permission: {default: 2}, data: { description: {value: "<p><em>Fondcombe, page 134</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Haubert des Nains", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesNainMontagnesBleues._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 120</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        await Item.create({ name: "Heaume d’Effroi", type: 'reward', img: 'systems/tor1e/assets/images/icons/reward.png', folder : dossierRecompensesNainMontagnesBleues._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 120</em></p>"}, type: {value: "tor1e.reward.groups.cultural"} } });
        






        console.log("-------------------------------------------------------------------");
        console.log("          ==> DEFAUTS D'OMBRE                                          ");
        console.log("-------------------------------------------------------------------");

        console.log("===== CREER LE DOSSIER DE BASE ====");
        let dossierDefautsOmbre = await Folder.create({
            name: 'Défauts',
            type: 'Item',
            color: '#2e0505',
            sorting: 'a',
            parent: dossierCaracteristiques
        });

        console.log("===== CREER LES SOUS-DOSSIERS ====");
        let dossierMaledictionVengeance = await Folder.create({
            name: 'Malédiction de Vengeance',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierDefautsOmbre._id
        });
        let dossierMalDuDragon = await Folder.create({
            name: 'Mal du Dragon',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierDefautsOmbre._id
        });
        let dossierAttraitDuPouvoir = await Folder.create({
            name: 'Attrait du Pouvoir',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierDefautsOmbre._id
        });
        let dossierAttraitDesSecrets = await Folder.create({
            name: 'Attrait des Secrets',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierDefautsOmbre._id
        });
        let dossierFolieItinerante = await Folder.create({
            name: 'Folie Itinérante',
            type: 'Item',
            color: '#181b26',
            sorting: 'a',
            parent: dossierDefautsOmbre._id
        });

        await Item.create({ name: "1. Rancunier", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierMaledictionVengeance._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 235</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "2. Brutal", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierMaledictionVengeance._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 235</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "3. Cruel", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierMaledictionVengeance._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 236</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "4. Meurtrier", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierMaledictionVengeance._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 236</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "1. Avide", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierMalDuDragon._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 236</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "2. Suspicieux", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierMalDuDragon._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 236</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "3. Malhonnête", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierMalDuDragon._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 236</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "4. Voleur", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierMalDuDragon._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 236</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "1. Amer", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierAttraitDuPouvoir._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 236</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "2. Arrogant", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierAttraitDuPouvoir._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 236</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "3. Présomptueux", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierAttraitDuPouvoir._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 236</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "4. Tyrannique", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierAttraitDuPouvoir._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 236</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "1. Hautain", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierAttraitDesSecrets._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 237</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "2. Méprisant", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierAttraitDesSecrets._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 237</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "3. Calculateur", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierAttraitDesSecrets._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 237</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "4. Fourbe", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierAttraitDesSecrets._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 237</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "1. Fainéant", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierFolieItinerante._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 237</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "2. Distrait", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierFolieItinerante._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 237</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "3. Indifférent", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierFolieItinerante._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 237</em></p>"}, group: {value: 'flaw'} } });
        await Item.create({ name: "4. Poltron", type: 'trait', img: 'systems/tor1e/assets/images/icons/shadow-weakness.png', folder : dossierFolieItinerante._id, permission: {default: 2}, data: { description: {value: "<p><em>Livre de base, page 237</em></p>"}, group: {value: 'flaw'} } });
        

        console.log("===== FIN CREATION DES CARACTERISTIQUES DES PERSONNAGES ====");
    }
}