export class Tor1eCompendiumPNJ {


    async creationPNJs() {
        console.log('===== CREATION DES PNJ ====');

        // DOSSIER DES PNJ
        let dossierPNJ = await Folder.create({
            name: "PNJ",
            type: 'Actor',
            sorting: 'a',
            parent: null
        });

        // DOSSIER DES REGIONS / PEUPLES
        let dossierHommesDesBois = await Folder.create({
            name: "Hommes des Bois",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        }); 
        let dossierBeornides = await Folder.create({
            name: "Beornides",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        }); 
        let dossierNains = await Folder.create({
            name: "Nains",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        });  
        let dossierHommesMauvais = await Folder.create({
            name: "PNJ mauvais",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        }); 
        let dossierEsgaroth = await Folder.create({
            name: "Esgaroth",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        });         
        let dossierDale = await Folder.create({
            name: "Dale",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        });        
        let dossierViglundings = await Folder.create({
            name: "Viglundings",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        });
        let dossierHommesAnduin = await Folder.create({
            name: "Vallées de l'Anduin",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        });       
        let dossierEriador = await Folder.create({
            name: "Hommes d'Eriador",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        });    
        let dossierElfesForetNoire = await Folder.create({
            name: "Elfes de la Forêt Noire",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        });  
        let dossierElfesFondcombe = await Folder.create({
            name: "Elfes de Fondcombe",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        });  
        let dossierElfesLorien = await Folder.create({
            name: "Elfes de la Lorien",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        });  
        let dossierHobbits = await Folder.create({
            name: "Hobbits",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        });  
        let dossierOrientaux = await Folder.create({
            name: "Orientaux",
            type: 'Actor',
            sorting: 'a',
            color: '#2e0505',
            parent: dossierPNJ._id
        });  


        // CREATION DES ACTORS
        let monActor1 = await this.ajouteUnPNJ(dossierHommesDesBois, "Hartfast, fils d’Harmunt", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 18, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor2 = await this.ajouteUnPNJ(dossierHommesDesBois, "Beranald le portier", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 3, 15, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor3 = await this.ajouteUnPNJ(dossierHommesDesBois, "Iwmud le chévrier", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 3, 15, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor4 = await this.ajouteUnPNJ(dossierHommesDesBois, "Gárhild la Renarde", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 16, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor5 = await this.ajouteUnPNJ(dossierHommesDesBois, "Les Filles de la Forêt", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor6 = await this.ajouteUnPNJ(dossierHommesMauvais, "Valdis (femme)", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 19, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.HOSTILE);
        let monActor7 = await this.ajouteUnPNJ(dossierHommesMauvais, "Sorcière de la Forêt Noire", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 20, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.HOSTILE);
        let monActor8 = await this.ajouteUnPNJ(dossierEsgaroth, "Garde de la ville du Lac", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 17, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor9 = await this.ajouteUnPNJ(dossierEsgaroth, "Marchand d’Esgaroth", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor10 = await this.ajouteUnPNJ(dossierEsgaroth, "Artisan (allume-feu)", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor11 = await this.ajouteUnPNJ(dossierEsgaroth, "Artisan (Batelier)", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor12 = await this.ajouteUnPNJ(dossierEsgaroth, "Artisan (Faune)", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor13 = await this.ajouteUnPNJ(dossierEsgaroth, "Artisan (Cuisine)", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor14 = await this.ajouteUnPNJ(dossierEsgaroth, "Artisan (Forge)", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor15 = await this.ajouteUnPNJ(dossierEsgaroth, "Artisan (Herboristerie)", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor16 = await this.ajouteUnPNJ(dossierEsgaroth, "Artisan (Jardinier", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor17 = await this.ajouteUnPNJ(dossierEsgaroth, "Artisan (Maçonnerie)", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor18 = await this.ajouteUnPNJ(dossierEsgaroth, "Artisan (Menuisier)", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor19 = await this.ajouteUnPNJ(dossierEsgaroth, "Capitaine du Guet", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 6, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor20 = await this.ajouteUnPNJ(dossierEsgaroth, "Homme du Guet", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor21 = await this.ajouteUnPNJ(dossierEsgaroth, "Conseiller communal", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor22 = await this.ajouteUnPNJ(dossierEsgaroth, "Soignante", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor23 = await this.ajouteUnPNJ(dossierEsgaroth, "Batelier d’Esgaroth", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor24 = await this.ajouteUnPNJ(dossierEsgaroth, "Archer de la Guilde", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 7, 19, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor25 = await this.ajouteUnPNJ(dossierDale, "Archer Royal de Dale", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 6, 20, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor26 = await this.ajouteUnPNJ(dossierBeornides, "Osred le cavalier", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 21, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor27 = await this.ajouteUnPNJ(dossierBeornides, "Túrin le ferblantier", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 2, 16, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor28 = await this.ajouteUnPNJ(dossierBeornides, "Gelvira touille-marmite", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 3, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor29 = await this.ajouteUnPNJ(dossierBeornides, "Ennalda, la Vierge à la lance", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 19, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor30 = await this.ajouteUnPNJ(dossierBeornides, "Forgeron nain", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 17, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor31 = await this.ajouteUnPNJ(dossierNains, "Bofri, fils de Bofur", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 3, 21, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor32 = await this.ajouteUnPNJ(dossierNains, "Notable nain", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 6, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor33 = await this.ajouteUnPNJ(dossierNains, "Frar l’imberbe", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 4, 22, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor34 = await this.ajouteUnPNJ(dossierNains, "Tholin le négociant", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 6, 20, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor35 = await this.ajouteUnPNJ(dossierHommesDesBois, "Geirbald le sororicide", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 17, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor36 = await this.ajouteUnPNJ(dossierHommesDesBois, "Ingomer Briseur-de-Haches", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 17, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor37 = await this.ajouteUnPNJ(dossierHommesDesBois, "Mogdred", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 6, 20, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor38 = await this.ajouteUnPNJ(dossierEsgaroth, "Hunald, assassin envouté", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor39 = await this.ajouteUnPNJ(dossierDale, "Noble bardide", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 6, 20, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor40 = await this.ajouteUnPNJ(dossierDale, "Elstan, premier capitaine de Dale", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 6, 20, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor41 = await this.ajouteUnPNJ(dossierDale, "Hommes d’Elstan", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 3, 12, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor42 = await this.ajouteUnPNJ(dossierEsgaroth, "Halbrech", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 16, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor43 = await this.ajouteUnPNJ(dossierViglundings, "Viglund", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 7, 23, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor44 = await this.ajouteUnPNJ(dossierViglundings, "Viglar, fils de Viglund", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 3, 19, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor45 = await this.ajouteUnPNJ(dossierViglundings, "Saviga le Gobelin", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 3, 12, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor46 = await this.ajouteUnPNJ(dossierViglundings, "Thunar", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 21, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor47 = await this.ajouteUnPNJ(dossierViglundings, "Aestid, fille de Viglund", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 20, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor48 = await this.ajouteUnPNJ(dossierHommesAnduin, "Amfossa la trappeuse", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 30, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor49 = await this.ajouteUnPNJ(dossierHommesAnduin, "Hwalda", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 3, 15, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor50 = await this.ajouteUnPNJ(dossierHommesAnduin, "Mab la fileuse", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 3, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor51 = await this.ajouteUnPNJ(dossierHommesAnduin, "Cruac le hors-la-loi", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 18, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor52 = await this.ajouteUnPNJ(dossierHommesAnduin, "Tom le noir", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 20, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor53 = await this.ajouteUnPNJ(dossierHommesAnduin, "Valderic", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 21, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor54 = await this.ajouteUnPNJ(dossierHommesAnduin, "Arciryas", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 20, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor55 = await this.ajouteUnPNJ(dossierHommesAnduin, "Vidugalum", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 30, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor56 = await this.ajouteUnPNJ(dossierEriador, "Ostley le fou", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 3, 15, "<p><em>Fondcombe, page 62</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor57 = await this.ajouteUnPNJ(dossierEriador, "Arbarad", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 6, 24, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor58 = await this.ajouteUnPNJ(dossierEriador, "Talandil", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 6, 20, "<p><em>Fondcombe, page 59</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor59 = await this.ajouteUnPNJ(dossierEriador, "Hiraval, fils d’Hirgeleb", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 7, 20, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor60 = await this.ajouteUnPNJ(dossierEriador, "Edrahil, fils d’Hiraval", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 3, 12, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor61 = await this.ajouteUnPNJ(dossierEriador, "Arnulf le Leofide", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 18, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor62 = await this.ajouteUnPNJ(dossierDale, "Gwina", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 3, 15, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor63 = await this.ajouteUnPNJ(dossierEriador, "Kyna", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 18, "<p><em>Fondcombe, page 65</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor64 = await this.ajouteUnPNJ(dossierEriador, "Bradan", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 19, "<p><em>Fondcombe, page 66</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor65 = await this.ajouteUnPNJ(dossierEsgaroth, "Elfe nautonier", "modules/fvtt-tor1e-compendium/icons/pnj-elfe.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor66 = await this.ajouteUnPNJ(dossierEsgaroth, "Emissaire elfe sylvain", "modules/fvtt-tor1e-compendium/icons/pnj-elfe.webp", 6, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor67 = await this.ajouteUnPNJ(dossierEsgaroth, "Sentinelle elfe sylvain", "modules/fvtt-tor1e-compendium/icons/pnj-elfe.webp", 6, 18, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor68 = await this.ajouteUnPNJ(dossierElfesForetNoire, "Le Roi Thranduil", "modules/fvtt-tor1e-compendium/icons/pnj-elfe.webp", 9, 60, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor69 = await this.ajouteUnPNJ(dossierElfesFondcombe, "Elrond", "modules/fvtt-tor1e-compendium/icons/pnj-elfe.webp", 9, 0, "<p><em>Fondcombe, page 13</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor70 = await this.ajouteUnPNJ(dossierElfesFondcombe, "Arwen Undómiel", "modules/fvtt-tor1e-compendium/icons/pnj-elfe.webp", 7, 0, "<p><em>Fondcombe, page 15</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor71 = await this.ajouteUnPNJ(dossierElfesFondcombe, "Glorfindel", "modules/fvtt-tor1e-compendium/icons/pnj-elfe.webp", 9, 0, "<p><em>Fondcombe, page 16</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor72 = await this.ajouteUnPNJ(dossierElfesFondcombe, "Aiwiel", "modules/fvtt-tor1e-compendium/icons/pnj-elfe.webp", 5, 0, "<p><em>Fondcombe, page 33</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor73 = await this.ajouteUnPNJ(dossierElfesFondcombe, "Gondril", "modules/fvtt-tor1e-compendium/icons/pnj-elfe.webp", 5, 18, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor74 = await this.ajouteUnPNJ(dossierElfesLorien, "Cendre", "modules/fvtt-tor1e-compendium/icons/pnj-elfe.webp", 8, 20, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor75 = await this.ajouteUnPNJ(dossierElfesLorien, "Haldir", "modules/fvtt-tor1e-compendium/icons/pnj-elfe.webp", 8, 20, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor76 = await this.ajouteUnPNJ(dossierHobbits, "Byrgol", "modules/fvtt-tor1e-compendium/icons/pnj-hobbit.webp", 3, 13, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor77 = await this.ajouteUnPNJ(dossierHobbits, "Folulf et Arnulf", "modules/fvtt-tor1e-compendium/icons/pnj-hobbit.webp", 4, 12, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor78 = await this.ajouteUnPNJ(dossierHobbits, "Elwin", "modules/fvtt-tor1e-compendium/icons/pnj-hobbit.webp", 4, 12, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor79 = await this.ajouteUnPNJ(dossierHobbits, "Fay", "modules/fvtt-tor1e-compendium/icons/pnj-hobbit.webp", 5, 10, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor80 = await this.ajouteUnPNJ(dossierHobbits, "Herbert", "modules/fvtt-tor1e-compendium/icons/pnj-hobbit.webp", 3, 12, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor81 = await this.ajouteUnPNJ(dossierNains, "Garde de Pierre", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 6, 18, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor82 = await this.ajouteUnPNJ(dossierNains, "Garde de  Fer", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 6, 26, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor83 = await this.ajouteUnPNJ(dossierNains, "Forgeron d’Erebor", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 5, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor84 = await this.ajouteUnPNJ(dossierNains, "Maçon d’Erebor", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 5, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor85 = await this.ajouteUnPNJ(dossierNains, "Daín Pied-d’Acier, Roi sous la Montagne", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 8, 28, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor86 = await this.ajouteUnPNJ(dossierNains, "Munin, Gardien des Archives", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor87 = await this.ajouteUnPNJ(dossierNains, "Glóin l’Emissaire", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 6, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor88 = await this.ajouteUnPNJ(dossierNains, "Óin le Guérisseur", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 6, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor89 = await this.ajouteUnPNJ(dossierNains, "Bifur le Marchand", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 6, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor90 = await this.ajouteUnPNJ(dossierNains, "Bofur le Mineur", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 5, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor91 = await this.ajouteUnPNJ(dossierNains, "Le Gros Bombur", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 5, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor92 = await this.ajouteUnPNJ(dossierNains, "Balin, l’Homme d’État", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 7, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor93 = await this.ajouteUnPNJ(dossierNains, "Dori le Marchand", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 5, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor94 = await this.ajouteUnPNJ(dossierNains, "Dwalin le Guerrier", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 6, 25, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor95 = await this.ajouteUnPNJ(dossierNains, "Ori le Scribe", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 5, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor96 = await this.ajouteUnPNJ(dossierNains, "Nori le Fabriquant de jouets", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 5, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor97 = await this.ajouteUnPNJ(dossierDale, "Marchand de Dale", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor98 = await this.ajouteUnPNJ(dossierDale, "Forgeron de la Voie de l’Enclume", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 5, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor99 = await this.ajouteUnPNJ(dossierDale, "Courtisan bardide", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor100 = await this.ajouteUnPNJ(dossierDale, "Garde de la ville de Dale", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 17, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor101 = await this.ajouteUnPNJ(dossierDale, "Commandant de la Garde de la ville de Dale", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 6, 20, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor102 = await this.ajouteUnPNJ(dossierDale, "Guérisseur de la Maison des Eaux", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, false, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor103 = await this.ajouteUnPNJ(dossierDale, "Bard, le Tueur de Dragon, roi de Dale", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 6, 26, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor104 = await this.ajouteUnPNJ(dossierDale, "La Reine Una la Belle", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor105 = await this.ajouteUnPNJ(dossierDale, "Brindal, voleuse entreprenante", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 0, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor106 = await this.ajouteUnPNJ(dossierDale, "Bryni, Seigneur de Rivebourg", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 19, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor107 = await this.ajouteUnPNJ(dossierDale, "Garrick de la Flèche Noire", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 19, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor108 = await this.ajouteUnPNJ(dossierDale, "Hakon, noble bardide", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 26, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor109 = await this.ajouteUnPNJ(dossierDale, "Erna", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 32, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor110 = await this.ajouteUnPNJ(dossierElfesForetNoire, "Ellaras le Quêteur", "modules/fvtt-tor1e-compendium/icons/pnj-elfe.webp", 6, 28, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor111 = await this.ajouteUnPNJ(dossierNains, "Thorin Heaume de-Pierre", "modules/fvtt-tor1e-compendium/icons/pnj-nain.webp", 6, 24, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor112 = await this.ajouteUnPNJ(dossierDale, "Alduna", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 3, 17, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor113 = await this.ajouteUnPNJ(dossierOrientaux, "Kajus l’Oriental", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 4, 13, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        let monActor114 = await this.ajouteUnPNJ(dossierDale, "Loore le Pisteur", "modules/fvtt-tor1e-compendium/icons/pnj-homme.webp", 5, 19, "<p><em>Voir dans le livre.</em></p>", false, true, CONST.TOKEN_DISPOSITIONS.NEUTRAL);
        

        // AJOUT DES COMPETENCES NOTABLES
        await this.ajouteCompetenceNotable(monActor1,"Présence",3, true);
        await this.ajouteCompetenceNotable(monActor1,"Intuition",3, false);
        await this.ajouteCompetenceNotable(monActor1,"Hache",3, true);
        await this.ajouteCompetenceNotable(monActor2,"Présence",3, false);
        await this.ajouteCompetenceNotable(monActor2,"Intuition",3, false);
        await this.ajouteCompetenceNotable(monActor2,"Courtoisie",3, false);
        await this.ajouteCompetenceNotable(monActor3,"Connaissances",3, false);
        await this.ajouteCompetenceNotable(monActor3,"Enigmes",3, true);
        await this.ajouteCompetenceNotable(monActor3,"Chant",2, false);
        await this.ajouteCompetenceNotable(monActor4,"Discrétion",4, false);
        await this.ajouteCompetenceNotable(monActor4,"Exploration",4, false);
        await this.ajouteCompetenceNotable(monActor4,"Lance",3, false);
        await this.ajouteCompetenceNotable(monActor5,"Discrétion",5, true);
        await this.ajouteCompetenceNotable(monActor5,"Persuasion",3, true);
        await this.ajouteCompetenceNotable(monActor6,"Chasse",3, false);
        await this.ajouteCompetenceNotable(monActor6,"Enigmes",4, false);
        await this.ajouteCompetenceNotable(monActor6,"Art de la guerre",2, false);
        await this.ajouteCompetenceNotable(monActor7,"Connaissances",5, false);
        await this.ajouteCompetenceNotable(monActor8,"Vigilance",2, false);
        await this.ajouteCompetenceNotable(monActor8,"Fouille",2, false);
        await this.ajouteCompetenceNotable(monActor8,"Lance",2, false);
        await this.ajouteCompetenceNotable(monActor9,"Intuition",2, false);
        await this.ajouteCompetenceNotable(monActor9,"Persuasion",3, false);
        await this.ajouteCompetenceNotable(monActor10,"Artisanat",4, true);
        await this.ajouteCompetenceNotable(monActor19,"Epée",2, true);
        await this.ajouteCompetenceNotable(monActor20,"Athlétisme",2, false);
        await this.ajouteCompetenceNotable(monActor20,"Vigilance",2, true);
        await this.ajouteCompetenceNotable(monActor21,"Courtoisie",3, true);
        await this.ajouteCompetenceNotable(monActor21,"Connaissances",2, false);
        await this.ajouteCompetenceNotable(monActor22,"Guérison",3, true);
        await this.ajouteCompetenceNotable(monActor22,"Connaissances",2, true);
        await this.ajouteCompetenceNotable(monActor23,"Athlétisme",3, true);
        await this.ajouteCompetenceNotable(monActor23,"Voyage",2, false);
        await this.ajouteCompetenceNotable(monActor24,"Vigilance",2, false);
        await this.ajouteCompetenceNotable(monActor24,"Grand arc",3, true);
        await this.ajouteCompetenceNotable(monActor25,"Vigilance",3, true);
        await this.ajouteCompetenceNotable(monActor25,"Grand arc",3, true);
        await this.ajouteCompetenceNotable(monActor26,"Athlétisme",3, false);
        await this.ajouteCompetenceNotable(monActor26,"Courtoisie",4, false);
        await this.ajouteCompetenceNotable(monActor26,"Lance",3, true);
        await this.ajouteCompetenceNotable(monActor27,"Voyage",3, false);
        await this.ajouteCompetenceNotable(monActor27,"Courtoisie",3, false);
        await this.ajouteCompetenceNotable(monActor27,"Enigmes",3, true);
        await this.ajouteCompetenceNotable(monActor28,"Artisanat",3, true);
        await this.ajouteCompetenceNotable(monActor28,"Courtoisie",4, false);
        await this.ajouteCompetenceNotable(monActor28,"Art de la guerre",3, false);
        await this.ajouteCompetenceNotable(monActor29,"Présence",4, false);
        await this.ajouteCompetenceNotable(monActor29,"Lance",3, true);
        await this.ajouteCompetenceNotable(monActor30,"Artisanat",4, true);
        await this.ajouteCompetenceNotable(monActor31,"Exploration",3, true);
        await this.ajouteCompetenceNotable(monActor31,"Artisanat",4, false);
        await this.ajouteCompetenceNotable(monActor32,"Présence",3, false);
        await this.ajouteCompetenceNotable(monActor32,"Courtoisie",3, false);
        await this.ajouteCompetenceNotable(monActor32,"Art de la guerre",2, false);
        await this.ajouteCompetenceNotable(monActor33,"Survie",3, true);
        await this.ajouteCompetenceNotable(monActor33,"Hache",3, false);
        await this.ajouteCompetenceNotable(monActor34,"Voyage",4, true);
        await this.ajouteCompetenceNotable(monActor34,"Persuasion",3, false);
        await this.ajouteCompetenceNotable(monActor34,"Courtoisie",3, false);
        await this.ajouteCompetenceNotable(monActor34,"Hache",2, false);
        await this.ajouteCompetenceNotable(monActor35,"Chasse",5, false);
        await this.ajouteCompetenceNotable(monActor35,"Hache à long manche",3, true);
        await this.ajouteCompetenceNotable(monActor36,"Présence",4, false);
        await this.ajouteCompetenceNotable(monActor36,"Art de la guerre",4, false);
        await this.ajouteCompetenceNotable(monActor36,"Hache",4, true);
        await this.ajouteCompetenceNotable(monActor37,"Art de la guerre",4, false);
        await this.ajouteCompetenceNotable(monActor37,"Présence",4, false);
        await this.ajouteCompetenceNotable(monActor37,"Epée",3, false);
        await this.ajouteCompetenceNotable(monActor38,"Vigilance",3, false);
        await this.ajouteCompetenceNotable(monActor38,"Epée",3, false);
        await this.ajouteCompetenceNotable(monActor38,"Grand arc",3, true);
        await this.ajouteCompetenceNotable(monActor39,"Inspiration",3, false);
        await this.ajouteCompetenceNotable(monActor39,"Persuasion",1, false);
        await this.ajouteCompetenceNotable(monActor39,"Epée longue",3, true);
        await this.ajouteCompetenceNotable(monActor40,"Vigilance",3, true);
        await this.ajouteCompetenceNotable(monActor40,"Art de la guerre",3, false);
        await this.ajouteCompetenceNotable(monActor40,"Inspiration",4, true);
        await this.ajouteCompetenceNotable(monActor40,"Epée",4, false);
        await this.ajouteCompetenceNotable(monActor40,"Lance",4, true);
        await this.ajouteCompetenceNotable(monActor41,"Personnalité",2, false);
        await this.ajouteCompetenceNotable(monActor41,"Déplacement",2, false);
        await this.ajouteCompetenceNotable(monActor41,"Perception",2, false);
        await this.ajouteCompetenceNotable(monActor41,"Survie",1, false);
        await this.ajouteCompetenceNotable(monActor41,"Coutume",3, true);
        await this.ajouteCompetenceNotable(monActor41,"Métier",2, false);
        await this.ajouteCompetenceNotable(monActor41,"Epée",2, false);
        await this.ajouteCompetenceNotable(monActor42,"Courtoisie",4, false);
        await this.ajouteCompetenceNotable(monActor42,"Discrétion",3, true);
        await this.ajouteCompetenceNotable(monActor43,"Art de la guerre",3, true);
        await this.ajouteCompetenceNotable(monActor43,"Présence",4, false);
        await this.ajouteCompetenceNotable(monActor43,"Hache",4, true);
        await this.ajouteCompetenceNotable(monActor44,"Chant",3, false);
        await this.ajouteCompetenceNotable(monActor44,"Persuasion",2, true);
        await this.ajouteCompetenceNotable(monActor44,"Hache",3, true);
        await this.ajouteCompetenceNotable(monActor45,"Intuition",3, true);
        await this.ajouteCompetenceNotable(monActor45,"Persuasion",3, false);
        await this.ajouteCompetenceNotable(monActor45,"Lance",3, true);
        await this.ajouteCompetenceNotable(monActor46,"Art de la guerre",4, false);
        await this.ajouteCompetenceNotable(monActor46,"Chasse",3, true);
        await this.ajouteCompetenceNotable(monActor46,"Hache",4, false);
        await this.ajouteCompetenceNotable(monActor46,"Arc",3, false);
        await this.ajouteCompetenceNotable(monActor47,"Discrétion",4, false);
        await this.ajouteCompetenceNotable(monActor47,"Enigmes",3, false);
        await this.ajouteCompetenceNotable(monActor48,"Chasse",3, true);
        await this.ajouteCompetenceNotable(monActor48,"Connaissances",3, false);
        await this.ajouteCompetenceNotable(monActor48,"Arc",3, true);
        await this.ajouteCompetenceNotable(monActor49,"Chasse",3, false);
        await this.ajouteCompetenceNotable(monActor49,"Exploration",3, true);
        await this.ajouteCompetenceNotable(monActor49,"Epée",2, true);
        await this.ajouteCompetenceNotable(monActor50,"Connaissances",4, false);
        await this.ajouteCompetenceNotable(monActor50,"Guérison",3, true);
        await this.ajouteCompetenceNotable(monActor50,"Artisanat",3, true);
        await this.ajouteCompetenceNotable(monActor51,"Art de la guerre",4, false);
        await this.ajouteCompetenceNotable(monActor51,"Enigmes",3, true);
        await this.ajouteCompetenceNotable(monActor52,"Présence",2, false);
        await this.ajouteCompetenceNotable(monActor52,"Chant",3, false);
        await this.ajouteCompetenceNotable(monActor52,"Hache à long manche",2, false);
        await this.ajouteCompetenceNotable(monActor53,"Exploration",3, false);
        await this.ajouteCompetenceNotable(monActor53,"Fouille",3, true);
        await this.ajouteCompetenceNotable(monActor54,"Connaissances",4, false);
        await this.ajouteCompetenceNotable(monActor54,"Guérison",4, true);
        await this.ajouteCompetenceNotable(monActor55,"Courtoisie",2, false);
        await this.ajouteCompetenceNotable(monActor55,"Présence",4, false);
        await this.ajouteCompetenceNotable(monActor55,"Connaissances",3, true);
        await this.ajouteCompetenceNotable(monActor56,"Fouille",3, false);
        await this.ajouteCompetenceNotable(monActor56,"Exploration",2, true);
        await this.ajouteCompetenceNotable(monActor57,"Vigilance",3, false);
        await this.ajouteCompetenceNotable(monActor57,"Voyage",2, false);
        await this.ajouteCompetenceNotable(monActor57,"Chasse",3, false);
        await this.ajouteCompetenceNotable(monActor57,"Grand arc",3, false);
        await this.ajouteCompetenceNotable(monActor58,"Connaissances",3, false);
        await this.ajouteCompetenceNotable(monActor58,"Chant",2, true);
        await this.ajouteCompetenceNotable(monActor58,"Epée longue",2, false);
        await this.ajouteCompetenceNotable(monActor59,"Présence",3, false);
        await this.ajouteCompetenceNotable(monActor59,"Voyage",3, false);
        await this.ajouteCompetenceNotable(monActor59,"Epée longue",3, true);
        await this.ajouteCompetenceNotable(monActor59,"Arc",2, true);
        await this.ajouteCompetenceNotable(monActor60,"Chant",2, false);
        await this.ajouteCompetenceNotable(monActor60,"Courtoisie",2, false);
        await this.ajouteCompetenceNotable(monActor60,"Guérison",1, true);
        await this.ajouteCompetenceNotable(monActor60,"Arc",3, false);
        await this.ajouteCompetenceNotable(monActor61,"Discrétion",4, false);
        await this.ajouteCompetenceNotable(monActor61,"Exploration",4, true);
        await this.ajouteCompetenceNotable(monActor61,"Lance",3, false);
        await this.ajouteCompetenceNotable(monActor62,"Athlétisme",3, false);
        await this.ajouteCompetenceNotable(monActor62,"Enigmes",3, true);
        await this.ajouteCompetenceNotable(monActor62,"Lance",2, false);
        await this.ajouteCompetenceNotable(monActor63,"Athlétisme",2, true);
        await this.ajouteCompetenceNotable(monActor63,"Exploration",2, false);
        await this.ajouteCompetenceNotable(monActor63,"Lance",3, false);
        await this.ajouteCompetenceNotable(monActor64,"Athlétisme",2, false);
        await this.ajouteCompetenceNotable(monActor64,"Voyage",2, false);
        await this.ajouteCompetenceNotable(monActor65,"Voyage",3, true);
        await this.ajouteCompetenceNotable(monActor66,"Courtoisie",3, true);
        await this.ajouteCompetenceNotable(monActor66,"Intuition",2, false);
        await this.ajouteCompetenceNotable(monActor67,"Athlétisme",2, true);
        await this.ajouteCompetenceNotable(monActor67,"Lance",2, false);
        await this.ajouteCompetenceNotable(monActor67,"Arc",3, false);
        await this.ajouteCompetenceNotable(monActor68,"Connaissances",4, true);
        await this.ajouteCompetenceNotable(monActor68,"Courtoisie",5, false);
        await this.ajouteCompetenceNotable(monActor68,"Lance",5, true);
        await this.ajouteCompetenceNotable(monActor68,"Arc",5, false);
        await this.ajouteCompetenceNotable(monActor69,"Connaissances",6, true);
        await this.ajouteCompetenceNotable(monActor69,"Guérison",5, true);
        await this.ajouteCompetenceNotable(monActor69,"Intuition",4, false);
        await this.ajouteCompetenceNotable(monActor70,"Artisanat",3, true);
        await this.ajouteCompetenceNotable(monActor70,"Courtoisie",4, false);
        await this.ajouteCompetenceNotable(monActor70,"Intuition",4, true);
        await this.ajouteCompetenceNotable(monActor71,"Présence",4, false);
        await this.ajouteCompetenceNotable(monActor71,"Exploration",4, true);
        await this.ajouteCompetenceNotable(monActor71,"Art de la guerre",5, true);
        await this.ajouteCompetenceNotable(monActor72,"Connaissances",4, true);
        await this.ajouteCompetenceNotable(monActor72,"Fouille",3, false);
        await this.ajouteCompetenceNotable(monActor73,"Artisanat",3, true);
        await this.ajouteCompetenceNotable(monActor73,"Epée longue",2, false);
        await this.ajouteCompetenceNotable(monActor74,"Présence",4, false);
        await this.ajouteCompetenceNotable(monActor74,"Connaissances",4, false);
        await this.ajouteCompetenceNotable(monActor74,"Chant",5, true);
        await this.ajouteCompetenceNotable(monActor75,"Vigilance",4, true);
        await this.ajouteCompetenceNotable(monActor75,"Discrétion",4, false);
        await this.ajouteCompetenceNotable(monActor75,"Grand arc",5, true);
        await this.ajouteCompetenceNotable(monActor76,"Discrétion",4, true);
        await this.ajouteCompetenceNotable(monActor76,"Voyage",2, false);
        await this.ajouteCompetenceNotable(monActor76,"Epée",2, false);
        await this.ajouteCompetenceNotable(monActor77,"Présence",2, false);
        await this.ajouteCompetenceNotable(monActor77,"Chasse",2, true);
        await this.ajouteCompetenceNotable(monActor77,"Hache",3, false);
        await this.ajouteCompetenceNotable(monActor77,"Arc",2, true);
        await this.ajouteCompetenceNotable(monActor78,"Inspiration",3, false);
        await this.ajouteCompetenceNotable(monActor78,"Voyage",1, true);
        await this.ajouteCompetenceNotable(monActor78,"Epée",2, true);
        await this.ajouteCompetenceNotable(monActor79,"Discrétion",3, false);
        await this.ajouteCompetenceNotable(monActor79,"Vigilance",3, true);
        await this.ajouteCompetenceNotable(monActor79,"Arc",2, true);
        await this.ajouteCompetenceNotable(monActor80,"Athlétisme",2, true);
        await this.ajouteCompetenceNotable(monActor80,"Présence",2, false);
        await this.ajouteCompetenceNotable(monActor80,"Lance",2, true);
        await this.ajouteCompetenceNotable(monActor80,"Dague",3, false);
        await this.ajouteCompetenceNotable(monActor81,"Hache",3, false);
        await this.ajouteCompetenceNotable(monActor81,"Vigilance",3, true);
        await this.ajouteCompetenceNotable(monActor81,"Fouille",2, false);
        await this.ajouteCompetenceNotable(monActor82,"Bigot",3, true);
        await this.ajouteCompetenceNotable(monActor83,"Artisanat",4, true);
        await this.ajouteCompetenceNotable(monActor84,"Artisanat",4, true);
        await this.ajouteCompetenceNotable(monActor85,"Art de la guerre",4, false);
        await this.ajouteCompetenceNotable(monActor85,"Courtoisie",3, false);
        await this.ajouteCompetenceNotable(monActor85,"Intuition",4, true);
        await this.ajouteCompetenceNotable(monActor85,"Persuasion",3, false);
        await this.ajouteCompetenceNotable(monActor85,"Hache",4, true);
        await this.ajouteCompetenceNotable(monActor86,"Connaissances",4, true);
        await this.ajouteCompetenceNotable(monActor87,"Courtoisie",3, false);
        await this.ajouteCompetenceNotable(monActor87,"Intuition",3, false);
        await this.ajouteCompetenceNotable(monActor87,"Persuasion",3, false);
        await this.ajouteCompetenceNotable(monActor88,"Inspiration",3, false);
        await this.ajouteCompetenceNotable(monActor88,"Voyage",3, false);
        await this.ajouteCompetenceNotable(monActor88,"Guérison",3, true);
        await this.ajouteCompetenceNotable(monActor89,"Vigilance",3, false);
        await this.ajouteCompetenceNotable(monActor89,"Voyage",3, false);
        await this.ajouteCompetenceNotable(monActor89,"Bigot",3, true);
        await this.ajouteCompetenceNotable(monActor90,"Exploration",3, false);
        await this.ajouteCompetenceNotable(monActor90,"Bigot",3, true);
        await this.ajouteCompetenceNotable(monActor91,"Courtoisie",3, false);
        await this.ajouteCompetenceNotable(monActor91,"Intuition",4, true);
        await this.ajouteCompetenceNotable(monActor91,"Chant",3, false);
        await this.ajouteCompetenceNotable(monActor92,"Vigilance",4, false);
        await this.ajouteCompetenceNotable(monActor92,"Courtoisie",4, false);
        await this.ajouteCompetenceNotable(monActor92,"Intuition",4, true);
        await this.ajouteCompetenceNotable(monActor92,"Connaissances",4, false);
        await this.ajouteCompetenceNotable(monActor93,"Persuasion",3, false);
        await this.ajouteCompetenceNotable(monActor93,"Chant",3, false);
        await this.ajouteCompetenceNotable(monActor93,"Hache",3, false);
        await this.ajouteCompetenceNotable(monActor94,"Présence",4, false);
        await this.ajouteCompetenceNotable(monActor94,"Hache",4, true);
        await this.ajouteCompetenceNotable(monActor95,"Exploration",3, false);
        await this.ajouteCompetenceNotable(monActor95,"Connaissances",4, true);
        await this.ajouteCompetenceNotable(monActor95,"Fouille",3, false);
        await this.ajouteCompetenceNotable(monActor96,"Artisanat",4, true);
        await this.ajouteCompetenceNotable(monActor96,"Enigmes",3, false);
        await this.ajouteCompetenceNotable(monActor96,"Chant",3, false);
        await this.ajouteCompetenceNotable(monActor97,"Intuition",2, false);
        await this.ajouteCompetenceNotable(monActor97,"Persuasion",3, false);
        await this.ajouteCompetenceNotable(monActor98,"Artisanat",4, true);
        await this.ajouteCompetenceNotable(monActor99,"Courtoisie",3, true);
        await this.ajouteCompetenceNotable(monActor99,"Inspiration",2, false);
        await this.ajouteCompetenceNotable(monActor99,"Persuasion",3, false);
        await this.ajouteCompetenceNotable(monActor100,"Vigilance",2, true);
        await this.ajouteCompetenceNotable(monActor100,"Fouille",2, false);
        await this.ajouteCompetenceNotable(monActor100,"Epée",2, false);
        await this.ajouteCompetenceNotable(monActor101,"Vigilance",2, true);
        await this.ajouteCompetenceNotable(monActor101,"Fouille",2, false);
        await this.ajouteCompetenceNotable(monActor101,"Epée",3, true);
        await this.ajouteCompetenceNotable(monActor102,"Guérison",3, false);
        await this.ajouteCompetenceNotable(monActor102,"Connaissances",2, false);
        await this.ajouteCompetenceNotable(monActor103,"Intuition",3, true);
        await this.ajouteCompetenceNotable(monActor103,"Chasse",3, false);
        await this.ajouteCompetenceNotable(monActor103,"Grand arc",5, true);
        await this.ajouteCompetenceNotable(monActor104,"Présence",3, false);
        await this.ajouteCompetenceNotable(monActor104,"Courtoisie",4, false);
        await this.ajouteCompetenceNotable(monActor104,"Intuition",4, false);
        await this.ajouteCompetenceNotable(monActor105,"Vigilance",3, false);
        await this.ajouteCompetenceNotable(monActor105,"Athlétisme",3, true);
        await this.ajouteCompetenceNotable(monActor105,"Discrétion",4, false);
        await this.ajouteCompetenceNotable(monActor106,"Présence",3, false);
        await this.ajouteCompetenceNotable(monActor106,"Chasse",3, false);
        await this.ajouteCompetenceNotable(monActor107,"Fouille",3, false);
        await this.ajouteCompetenceNotable(monActor107,"Voyage",3, false);
        await this.ajouteCompetenceNotable(monActor107,"Arc",4, true);
        await this.ajouteCompetenceNotable(monActor108,"Intuition",2, true);
        await this.ajouteCompetenceNotable(monActor108,"Persuasion",4, true);
        await this.ajouteCompetenceNotable(monActor108,"Discrétion",3, false);
        await this.ajouteCompetenceNotable(monActor109,"Chasse",3, true);
        await this.ajouteCompetenceNotable(monActor109,"Connaissances",3, false);
        await this.ajouteCompetenceNotable(monActor109,"Grand arc",4, true);
        await this.ajouteCompetenceNotable(monActor110,"Connaissances",4, false);
        await this.ajouteCompetenceNotable(monActor110,"Fouille",3, true);
        await this.ajouteCompetenceNotable(monActor110,"Arc",4, true);
        await this.ajouteCompetenceNotable(monActor111,"Présence",3, false);
        await this.ajouteCompetenceNotable(monActor111,"Artisanat",3, true);
        await this.ajouteCompetenceNotable(monActor111,"Bigot",4, false);
        await this.ajouteCompetenceNotable(monActor112,"Courtoisie",4, true);
        await this.ajouteCompetenceNotable(monActor112,"Persuasion",3, false);
        await this.ajouteCompetenceNotable(monActor113,"Courtoisie",4, false);
        await this.ajouteCompetenceNotable(monActor113,"Intuition",4, true);
        await this.ajouteCompetenceNotable(monActor113,"Voyage",3, false);
        await this.ajouteCompetenceNotable(monActor114,"Chasse",4, false);
        await this.ajouteCompetenceNotable(monActor114,"Arc",3, false);
        await this.ajouteCompetenceNotable(monActor114,"Voyage",4, false);

        
        
        // AJOUT DES TRAITS
        await this.ajouteTrait(monActor1,"Montagnard","", "");
        await this.ajouteTrait(monActor1,"Connaissance des ennemis [race]","Connaissance des ennemis [Gobelins]", "");
        await this.ajouteTrait(monActor1,"Endurci","", "");
        await this.ajouteTrait(monActor1,"Entêté","", "");
        await this.ajouteTrait(monActor1,"Méfiant","Méfiant [Elfes]", "");
        await this.ajouteTrait(monActor2,"Folklore","", "");
        await this.ajouteTrait(monActor2,"Tradition ancienne","", "");
        await this.ajouteTrait(monActor2,"Honorable","", "");
        await this.ajouteTrait(monActor2,"Prudent","", "");
        await this.ajouteTrait(monActor3,"Fumer","", "");
        await this.ajouteTrait(monActor3,"Médecine","", "");
        await this.ajouteTrait(monActor3,"Astucieux","", "");
        await this.ajouteTrait(monActor3,"Cachottier","", "");
        await this.ajouteTrait(monActor3,"Patient","", "");
        await this.ajouteTrait(monActor4,"Connaissance des ennemis [race]","Connaissance des ennemis [Araignées]", "");
        await this.ajouteTrait(monActor4,"Connaissance de la faune","", "");
        await this.ajouteTrait(monActor4,"Courtois","", "");
        await this.ajouteTrait(monActor4,"Curieux","", "");
        await this.ajouteTrait(monActor4,"Malin","", "");
        await this.ajouteTrait(monActor5,"Connaissances régionales","Connaissances régionales : Foret Noire", "");
        await this.ajouteTrait(monActor5,"Natation","", "");
        await this.ajouteTrait(monActor5,"Beau","Belle", "");
        await this.ajouteTrait(monActor5,"Insaisissable","", "");
        await this.ajouteTrait(monActor6,"Connaissances régionales","Connaissances régionales : Esgaroth", "");
        await this.ajouteTrait(monActor6,"Manigances","", "");
        await this.ajouteTrait(monActor6,"Déterminé","", "");
        await this.ajouteTrait(monActor6,"Beau","Belle", "");
        await this.ajouteTrait(monActor6,"Malin","", "");
        await this.ajouteTrait(monActor7,"Conscience de l-Ombre","Conscience de l’Ombre", "");
        await this.ajouteTrait(monActor7,"Tradition ancienne","", "");
        await this.ajouteTrait(monActor7,"Cachottier","", "");
        await this.ajouteTrait(monActor7,"Majestueux","", "");
        await this.ajouteTrait(monActor8,"Méfiant","", "");
        await this.ajouteTrait(monActor8,"Sévère","", "");
        await this.ajouteTrait(monActor9,"Commerce","", "");
        await this.ajouteTrait(monActor9,"Prudent","", "");
        await this.ajouteTrait(monActor10,"Allume-feu","", "");
        await this.ajouteTrait(monActor10,"Bourru","", "");
        await this.ajouteTrait(monActor10,"Déterminé","", "");
        await this.ajouteTrait(monActor11,"Batelier","", "");
        await this.ajouteTrait(monActor12,"Connaissance de la faune","", "");
        await this.ajouteTrait(monActor13,"Cuisine","", "");
        await this.ajouteTrait(monActor14,"Forge","", "");
        await this.ajouteTrait(monActor15,"Herboristerie","", "");
        await this.ajouteTrait(monActor16,"Jardinier","", "");
        await this.ajouteTrait(monActor17,"Maçonnerie","", "");
        await this.ajouteTrait(monActor18,"Menuisier","", "");
        await this.ajouteTrait(monActor11,"Bourru","", "");
        await this.ajouteTrait(monActor11,"Déterminé","", "");
        await this.ajouteTrait(monActor12,"Bourru","", "");
        await this.ajouteTrait(monActor12,"Déterminé","", "");
        await this.ajouteTrait(monActor13,"Bourru","", "");
        await this.ajouteTrait(monActor13,"Déterminé","", "");
        await this.ajouteTrait(monActor14,"Bourru","", "");
        await this.ajouteTrait(monActor14,"Déterminé","", "");
        await this.ajouteTrait(monActor15,"Bourru","", "");
        await this.ajouteTrait(monActor15,"Déterminé","", "");
        await this.ajouteTrait(monActor16,"Bourru","", "");
        await this.ajouteTrait(monActor16,"Déterminé","", "");
        await this.ajouteTrait(monActor17,"Bourru","", "");
        await this.ajouteTrait(monActor17,"Déterminé","", "");
        await this.ajouteTrait(monActor18,"Bourru","", "");
        await this.ajouteTrait(monActor18,"Déterminé","", "");
        await this.ajouteTrait(monActor19,"Observateur","", "");
        await this.ajouteTrait(monActor19,"Patient","", "");
        await this.ajouteTrait(monActor20,"Circonspect","", "");
        await this.ajouteTrait(monActor20,"Résistant","", "");
        await this.ajouteTrait(monActor21,"Astucieux","", "");
        await this.ajouteTrait(monActor21,"Courtois","", "");
        await this.ajouteTrait(monActor21,"Commerce","", "");
        await this.ajouteTrait(monActor22,"Miséricordieux","", "");
        await this.ajouteTrait(monActor22,"Patient","", "");
        await this.ajouteTrait(monActor22,"Herboristerie","", "");
        await this.ajouteTrait(monActor22,"Médecine","", "");
        await this.ajouteTrait(monActor23,"Batelier","", "");
        await this.ajouteTrait(monActor23,"Résistant","", "");
        await this.ajouteTrait(monActor23,"Vue perçante","", "");
        await this.ajouteTrait(monActor24,"Connaissance des ennemis [race]","Connaissance des ennemis [Dragons]", "");
        await this.ajouteTrait(monActor24,"Endurci","", "");
        await this.ajouteTrait(monActor24,"Loyal","", "");
        await this.ajouteTrait(monActor25,"Menuisier","", "");
        await this.ajouteTrait(monActor25,"Energique","", "");
        await this.ajouteTrait(monActor25,"Vue perçante","", "");
        await this.ajouteTrait(monActor26,"Commerce","", "");
        await this.ajouteTrait(monActor26,"Folklore","", "");
        await this.ajouteTrait(monActor26,"Généreux","", "");
        await this.ajouteTrait(monActor26,"Majestueux","", "");
        await this.ajouteTrait(monActor27,"Commerce","", "");
        await this.ajouteTrait(monActor27,"Forge","", "");
        await this.ajouteTrait(monActor27,"Jovial","", "");
        await this.ajouteTrait(monActor27,"Entêté","", "");
        await this.ajouteTrait(monActor28,"Cuisine","", "");
        await this.ajouteTrait(monActor28,"Folklore","", "");
        await this.ajouteTrait(monActor28,"Généreux","", "");
        await this.ajouteTrait(monActor28,"Ouïe fine","", "");
        await this.ajouteTrait(monActor29,"Connaissance des ennemis [race]","Connaissance des ennemis [Loups sauvages]", "");
        await this.ajouteTrait(monActor29,"Connaissances régionales","Connaissances régionales [Foret Noire]", "");
        await this.ajouteTrait(monActor29,"Observateur","", "");
        await this.ajouteTrait(monActor29,"Sévère","", "");
        await this.ajouteTrait(monActor29,"Vengeur","", "");
        await this.ajouteTrait(monActor30,"Cachottier","", "");
        await this.ajouteTrait(monActor30,"Sinistre","", "");
        await this.ajouteTrait(monActor30,"Forge","", "");
        await this.ajouteTrait(monActor31,"Maçonnerie","", "");
        await this.ajouteTrait(monActor31,"Cartographie","", "");
        await this.ajouteTrait(monActor31,"Energique","", "");
        await this.ajouteTrait(monActor31,"Audacieux","", "");
        await this.ajouteTrait(monActor32,"Bourru","", "");
        await this.ajouteTrait(monActor32,"Courageux","", "");
        await this.ajouteTrait(monActor32,"Majestueux","", "");
        await this.ajouteTrait(monActor32,"Tradition ancienne","", "");
        await this.ajouteTrait(monActor33,"Commerce","", "");
        await this.ajouteTrait(monActor33,"Troglodyte","", "");
        await this.ajouteTrait(monActor33,"Excentrique","", "");
        await this.ajouteTrait(monActor33,"Vengeur","", "");
        await this.ajouteTrait(monActor34,"Commerce","", "");
        await this.ajouteTrait(monActor34,"Folklore","", "");
        await this.ajouteTrait(monActor34,"Tradition ancienne","", "");
        await this.ajouteTrait(monActor34,"Jovial","", "");
        await this.ajouteTrait(monActor35,"Connaissance de la faune","", "");
        await this.ajouteTrait(monActor35,"Connaissances régionales","Connaissances régionales [Foret Noire]", "");
        await this.ajouteTrait(monActor35,"Endurci","", "");
        await this.ajouteTrait(monActor35,"Sincère","", "");
        await this.ajouteTrait(monActor36,"Connaissance des ennemis [race]","Connaissance des ennemis [Araignées]", "");
        await this.ajouteTrait(monActor36,"Connaissances régionales","Connaissances régionales [Foret Noire]", "");
        await this.ajouteTrait(monActor36,"Franc","", "");
        await this.ajouteTrait(monActor36,"Loyal","", "");
        await this.ajouteTrait(monActor37,"Connaissances régionales","Connaissances régionales [Foret Noire]", "");
        await this.ajouteTrait(monActor37,"Conscience de l-Ombre","Conscience de l’Ombre", "");
        await this.ajouteTrait(monActor37,"Courageux","", "");
        await this.ajouteTrait(monActor37,"Téméraire","", "");
        await this.ajouteTrait(monActor38,"Méfiant","", "");
        await this.ajouteTrait(monActor38,"Sinistre","", "");
        await this.ajouteTrait(monActor39,"Connaissances régionales","Connaissances régionales [Dale]", "");
        await this.ajouteTrait(monActor39,"Courageux","", "");
        await this.ajouteTrait(monActor39,"Fier","", "");
        await this.ajouteTrait(monActor40,"Connaissances régionales","Connaissances régionales [Dale]", "");
        await this.ajouteTrait(monActor40,"Conte","", "");
        await this.ajouteTrait(monActor40,"Jovial","", "");
        await this.ajouteTrait(monActor40,"Sincère","", "");
        await this.ajouteTrait(monActor42,"Commerce","", "");
        await this.ajouteTrait(monActor42,"Connaissances elfiques","", "");
        await this.ajouteTrait(monActor42,"Jovial","", "");
        await this.ajouteTrait(monActor42,"Cachottier","", "");
        await this.ajouteTrait(monActor43,"Connaissance des ennemis [race]","Connaissance des ennemis [Gobelins]", "");
        await this.ajouteTrait(monActor43,"Connaissances régionales","Connaissances régionales [Dale]", "");
        await this.ajouteTrait(monActor43,"Cruel","", "");
        await this.ajouteTrait(monActor43,"Sévère","", "");
        await this.ajouteTrait(monActor43,"Vengeur","", "");
        await this.ajouteTrait(monActor44,"Noceur","", "");
        await this.ajouteTrait(monActor44,"Téméraire","", "");
        await this.ajouteTrait(monActor44,"Robuste","", "");
        await this.ajouteTrait(monActor45,"Connaissance des ennemis [race]","Connaissance des ennemis [Gobelins]", "");
        await this.ajouteTrait(monActor45,"Conte","", "");
        await this.ajouteTrait(monActor45,"Malin","", "");
        await this.ajouteTrait(monActor45,"Patient","", "");
        await this.ajouteTrait(monActor46,"Connaissances régionales","Connaissances régionales [Foret Noire]", "");
        await this.ajouteTrait(monActor46,"Allume-feu","", "");
        await this.ajouteTrait(monActor46,"Beau","", "");
        await this.ajouteTrait(monActor46,"Vengeur","", "");
        await this.ajouteTrait(monActor47,"Commerce","", "");
        await this.ajouteTrait(monActor47,"Folklore","", "");
        await this.ajouteTrait(monActor47,"Astucieux","", "");
        await this.ajouteTrait(monActor47,"Déterminé","", "");
        await this.ajouteTrait(monActor48,"Connaissances régionales","Connaissances régionales [Anduin]", "");
        await this.ajouteTrait(monActor48,"Natation","", "");
        await this.ajouteTrait(monActor48,"Insaisissable","", "");
        await this.ajouteTrait(monActor48,"Loyal","", "");
        await this.ajouteTrait(monActor48,"Patient","", "");
        await this.ajouteTrait(monActor49,"Connaissance des ennemis [race]","Connaissance des ennemis [Loups sauvages]", "");
        await this.ajouteTrait(monActor49,"Menuisier","", "");
        await this.ajouteTrait(monActor49,"Circonspect","", "");
        await this.ajouteTrait(monActor49,"Méfiant","", "");
        await this.ajouteTrait(monActor50,"Conscience de l-Ombre","Conscience de l’Ombre", "");
        await this.ajouteTrait(monActor50,"Rimes de savoir","", "");
        await this.ajouteTrait(monActor50,"Tissage","", "");
        await this.ajouteTrait(monActor50,"Astucieux","", "");
        await this.ajouteTrait(monActor50,"Cachottier","", "");
        await this.ajouteTrait(monActor51,"Monte-en-l-air","Monte-en-l’air", "");
        await this.ajouteTrait(monActor51,"Montagnard","", "");
        await this.ajouteTrait(monActor51,"Jovial","", "");
        await this.ajouteTrait(monActor51,"Malin","", "");
        await this.ajouteTrait(monActor52,"Connaissance de la faune","", "");
        await this.ajouteTrait(monActor52,"Jardinier","", "");
        await this.ajouteTrait(monActor52,"Grand","", "");
        await this.ajouteTrait(monActor52,"Bourru","", "");
        await this.ajouteTrait(monActor53,"Connaissance de la faune","", "");
        await this.ajouteTrait(monActor53,"Conscience de l-Ombre","Conscience de l’Ombre", "");
        await this.ajouteTrait(monActor53,"Curieux","", "");
        await this.ajouteTrait(monActor53,"Endurci","", "");
        await this.ajouteTrait(monActor53,"Impétueux","", "");
        await this.ajouteTrait(monActor54,"Conscience de l-Ombre","Conscience de l’Ombre", "");
        await this.ajouteTrait(monActor54,"Herboristerie","", "");
        await this.ajouteTrait(monActor54,"Inébranlable","", "");
        await this.ajouteTrait(monActor54,"Majestueux","", "");
        await this.ajouteTrait(monActor55,"Commerce","", "");
        await this.ajouteTrait(monActor55,"Conscience de l-Ombre","Conscience de l’Ombre", "");
        await this.ajouteTrait(monActor55,"Entêté","", "");
        await this.ajouteTrait(monActor55,"Fier","", "");
        await this.ajouteTrait(monActor55,"Majestueux","", "");
        await this.ajouteTrait(monActor56,"Tradition ancienne","", "");
        await this.ajouteTrait(monActor56,"Troglodyte","", "");
        await this.ajouteTrait(monActor56,"Bourru","", "");
        await this.ajouteTrait(monActor56,"Cachottier","", "");
        await this.ajouteTrait(monActor56,"Endurci","", "");
        await this.ajouteTrait(monActor57,"Allume-feu","", "");
        await this.ajouteTrait(monActor57,"Conscience de l-Ombre","Conscience de l’Ombre", "");
        await this.ajouteTrait(monActor57,"Tradition d-Arnor","Tradition d’Arnor", "");
        await this.ajouteTrait(monActor57,"Impétueux","", "");
        await this.ajouteTrait(monActor57,"Sinistre","", "");
        await this.ajouteTrait(monActor58,"Fumer","", "");
        await this.ajouteTrait(monActor58,"Tradition d-Arnor","Tradition d’Arnor", "");
        await this.ajouteTrait(monActor58,"Déterminé","", "");
        await this.ajouteTrait(monActor58,"Insaisissable","", "");
        await this.ajouteTrait(monActor59,"Connaissances régionales","Connaissances régionales [Arnor]", "");
        await this.ajouteTrait(monActor59,"Conscience de l-Ombre","Conscience de l’Ombre", "");
        await this.ajouteTrait(monActor59,"Amer","", "");
        await this.ajouteTrait(monActor59,"Présomptueux","", "");
        await this.ajouteTrait(monActor60,"Connaissance de la faune","", "");
        await this.ajouteTrait(monActor60,"Allume-feu","", "");
        await this.ajouteTrait(monActor60,"Energique","", "");
        await this.ajouteTrait(monActor60,"Inébranlable","", "");
        await this.ajouteTrait(monActor61,"Connaissances régionales","Connaissances régionales [Anduin]", "");
        await this.ajouteTrait(monActor61,"Connaissance des ennemis [race]","Connaissance des ennemis [Orcs]", "");
        await this.ajouteTrait(monActor61,"Fou","", "");
        await this.ajouteTrait(monActor61,"Vengeur","", "");
        await this.ajouteTrait(monActor62,"Batelier","", "");
        await this.ajouteTrait(monActor62,"Commerce","", "");
        await this.ajouteTrait(monActor62,"Jovial","", "");
        await this.ajouteTrait(monActor62,"Preste","", "");
        await this.ajouteTrait(monActor63,"Chasse","", "");
        await this.ajouteTrait(monActor63,"Pêche","", "");
        await this.ajouteTrait(monActor63,"Curieux","Curieuse", "");
        await this.ajouteTrait(monActor63,"Passionné","Passionnée", "");
        await this.ajouteTrait(monActor64,"Connaissance de la faune","", "");
        await this.ajouteTrait(monActor64,"Herboristerie","", "");
        await this.ajouteTrait(monActor64,"Vue perçante","", "");
        await this.ajouteTrait(monActor64,"Rapide","", "");
        await this.ajouteTrait(monActor65,"Batelier","", "");
        await this.ajouteTrait(monActor65,"Fier","", "");
        await this.ajouteTrait(monActor66,"Connaissances elfiques","", "");
        await this.ajouteTrait(monActor66,"Cachottier","", "");
        await this.ajouteTrait(monActor66,"Malin","", "");
        await this.ajouteTrait(monActor66,"Méfiant","", "");
        await this.ajouteTrait(monActor67,"Inébranlable","", "");
        await this.ajouteTrait(monActor67,"Vue perçante","", "");
        await this.ajouteTrait(monActor68,"Connaissances elfiques","", "");
        await this.ajouteTrait(monActor68,"Tradition ancienne","", "");
        await this.ajouteTrait(monActor68,"Fier","", "");
        await this.ajouteTrait(monActor68,"Majestueux","", "");
        await this.ajouteTrait(monActor69,"Connaissances elfiques","", "");
        await this.ajouteTrait(monActor69,"Conscience de l-Ombre","Conscience de l’Ombre", "");
        await this.ajouteTrait(monActor69,"Médecine","", "");
        await this.ajouteTrait(monActor69,"Généreux","", "");
        await this.ajouteTrait(monActor69,"Majestueux","", "");
        await this.ajouteTrait(monActor69,"Observateur","", "");
        await this.ajouteTrait(monActor70,"Connaissances elfiques","", "");
        await this.ajouteTrait(monActor70,"Conscience de l-Ombre","Conscience de l’Ombre", "");
        await this.ajouteTrait(monActor70,"Broderie","", "");
        await this.ajouteTrait(monActor70,"Beau","Belle", "");
        await this.ajouteTrait(monActor70,"Miséricordieux","Miséricordieuse", "");
        await this.ajouteTrait(monActor70,"Sincère","", "");
        await this.ajouteTrait(monActor71,"Connaissance de la faune","", "");
        await this.ajouteTrait(monActor71,"Connaissances elfiques","", "");
        await this.ajouteTrait(monActor71,"Beau","", "");
        await this.ajouteTrait(monActor71,"Energique","", "");
        await this.ajouteTrait(monActor71,"Majestueux","", "");
        await this.ajouteTrait(monActor72,"Connaissances elfiques","", "");
        await this.ajouteTrait(monActor72,"Ennemi de Sauron","", "");
        await this.ajouteTrait(monActor72,"Majestueux","Majestueuse", "");
        await this.ajouteTrait(monActor72,"Patient","Patiente", "");
        await this.ajouteTrait(monActor72,"Vue perçante","", "");
        await this.ajouteTrait(monActor73,"Maçonnerie","", "");
        await this.ajouteTrait(monActor73,"Courageux","Courageuse", "");
        await this.ajouteTrait(monActor73,"Insaisissable","", "");
        await this.ajouteTrait(monActor74,"Conscience de l-Ombre","Conscience de l’Ombre", "");
        await this.ajouteTrait(monActor74,"Connaissances elfiques","", "");
        await this.ajouteTrait(monActor74,"Circonspect","", "");
        await this.ajouteTrait(monActor74,"Fier","", "");
        await this.ajouteTrait(monActor74,"Sinistre","", "");
        await this.ajouteTrait(monActor75,"Connaissance des ennemis [race]","Connaissance des ennemis [Gobelins]", "");
        await this.ajouteTrait(monActor75,"Folklore","", "");
        await this.ajouteTrait(monActor75,"Méfiant","", "");
        await this.ajouteTrait(monActor75,"Vue perçante","", "");
        await this.ajouteTrait(monActor76,"Pêche","", "");
        await this.ajouteTrait(monActor76,"Troglodyte","", "");
        await this.ajouteTrait(monActor76,"Passionné","", "");
        await this.ajouteTrait(monActor76,"Preste","", "");
        await this.ajouteTrait(monActor76,"Téméraire","", "");
        await this.ajouteTrait(monActor77,"Folklore","", "");
        await this.ajouteTrait(monActor77,"Rapide","Rapide (Folulf)", "");
        await this.ajouteTrait(monActor77,"Circonspect","Circonspect (Arnulf)", "");
        await this.ajouteTrait(monActor78,"Connaissances régionales","Connaissances régionales [Eriador]", "");
        await this.ajouteTrait(monActor78,"Passionné","", "");
        await this.ajouteTrait(monActor78,"Sincère","", "");
        await this.ajouteTrait(monActor79,"Monte-en-l-air","Monte-en-l’air", "");
        await this.ajouteTrait(monActor79,"Bourru","Bourrue", "");
        await this.ajouteTrait(monActor79,"Vue perçante","", "");
        await this.ajouteTrait(monActor80,"Connaissance des ennemis [race]","Connaissance des ennemis [Loups]", "");
        await this.ajouteTrait(monActor80,"Rancunier","", "");
        await this.ajouteTrait(monActor80,"Brutal","", "");
        await this.ajouteTrait(monActor81,"Connaissances régionales","Connaissances régionales [Erebor]", "");
        await this.ajouteTrait(monActor81,"Robuste","", "");
        await this.ajouteTrait(monActor81,"Sévère","", "");
        await this.ajouteTrait(monActor82,"Connaissances régionales","Connaissances régionales [Erebor]", "");
        await this.ajouteTrait(monActor82,"Connaissance des ennemis [race]","Connaissance des ennemis [Orcs]", "");
        await this.ajouteTrait(monActor82,"Sinistre","", "");
        await this.ajouteTrait(monActor82,"Résistant","", "");
        await this.ajouteTrait(monActor82,"Honorable","", "");
        await this.ajouteTrait(monActor83,"Forge","", "");
        await this.ajouteTrait(monActor83,"Inébranlable","", "");
        await this.ajouteTrait(monActor83,"Cachottier","", "");
        await this.ajouteTrait(monActor84,"Maçonnerie","", "");
        await this.ajouteTrait(monActor84,"Inébranlable","", "");
        await this.ajouteTrait(monActor84,"Cachottier","", "");
        await this.ajouteTrait(monActor85,"Connaissances régionales","Connaissances régionales [Erebor]", "");
        await this.ajouteTrait(monActor85,"Connaissance des ennemis [race]","Connaissance des ennemis [Orcs]", "");
        await this.ajouteTrait(monActor85,"Fier","", "");
        await this.ajouteTrait(monActor85,"Sévère","", "");
        await this.ajouteTrait(monActor86,"Tradition ancienne","", "");
        await this.ajouteTrait(monActor86,"Bourru","", "");
        await this.ajouteTrait(monActor86,"Inébranlable","", "");
        await this.ajouteTrait(monActor87,"Commerce","", "");
        await this.ajouteTrait(monActor87,"Courtois","", "");
        await this.ajouteTrait(monActor87,"Vengeur","", "");
        await this.ajouteTrait(monActor88,"Allume-feu","", "");
        await this.ajouteTrait(monActor88,"Herboristerie","", "");
        await this.ajouteTrait(monActor88,"Courageux","", "");
        await this.ajouteTrait(monActor88,"Franc","", "");
        await this.ajouteTrait(monActor89,"Forge","", "");
        await this.ajouteTrait(monActor89,"Maçonnerie","", "");
        await this.ajouteTrait(monActor89,"Energique","", "");
        await this.ajouteTrait(monActor89,"Circonspect","", "");
        await this.ajouteTrait(monActor90,"Montagnard","", "");
        await this.ajouteTrait(monActor90,"Forge","", "");
        await this.ajouteTrait(monActor90,"Audacieux","", "");
        await this.ajouteTrait(monActor90,"Robuste","", "");
        await this.ajouteTrait(monActor91,"Cuisine","", "");
        await this.ajouteTrait(monActor91,"Herboristerie","", "");
        await this.ajouteTrait(monActor91,"Malin","", "");
        await this.ajouteTrait(monActor91,"Résistant","", "");
        await this.ajouteTrait(monActor92,"Connaissance de la montagne","", "");
        await this.ajouteTrait(monActor92,"Connaissance des ennemis [race]","Connaissance des ennemis [Orcs]", "");
        await this.ajouteTrait(monActor92,"Rimes de savoir","", "");
        await this.ajouteTrait(monActor92,"Audacieux","", "");
        await this.ajouteTrait(monActor92,"Courtois","", "");
        await this.ajouteTrait(monActor93,"Connaissances régionales","Connaissances régionales [Erebor]", "");
        await this.ajouteTrait(monActor93,"Commerce","", "");
        await this.ajouteTrait(monActor93,"Prudent","", "");
        await this.ajouteTrait(monActor93,"Sincère","", "");
        await this.ajouteTrait(monActor94,"Connaissances régionales","Connaissances régionales [Erebor]", "");
        await this.ajouteTrait(monActor94,"Connaissance des ennemis [race]","Connaissance des ennemis [Orcs]", "");
        await this.ajouteTrait(monActor94,"Endurci","", "");
        await this.ajouteTrait(monActor94,"Méfiant","", "");
        await this.ajouteTrait(monActor95,"Tradition ancienne","", "");
        await this.ajouteTrait(monActor95,"Conte","", "");
        await this.ajouteTrait(monActor95,"Curieux","", "");
        await this.ajouteTrait(monActor95,"Loyal","", "");
        await this.ajouteTrait(monActor96,"Forge","", "");
        await this.ajouteTrait(monActor96,"Fumer","", "");
        await this.ajouteTrait(monActor96,"Menuisier","", "");
        await this.ajouteTrait(monActor96,"Energique","", "");
        await this.ajouteTrait(monActor96,"Preste","", "");
        await this.ajouteTrait(monActor97,"Commerce","", "");
        await this.ajouteTrait(monActor97,"Energique","", "");
        await this.ajouteTrait(monActor97,"Franc","", "");
        await this.ajouteTrait(monActor98,"Forge","", "");
        await this.ajouteTrait(monActor98,"Astucieux","", "");
        await this.ajouteTrait(monActor98,"Bourru","", "");
        await this.ajouteTrait(monActor99,"Connaissances régionales","Connaissances régionales [Dale]", "");
        await this.ajouteTrait(monActor99,"Malin","", "");
        await this.ajouteTrait(monActor99,"Majestueux","", "");
        await this.ajouteTrait(monActor100,"Prudent","", "");
        await this.ajouteTrait(monActor100,"Circonspect","", "");
        await this.ajouteTrait(monActor101,"Endurci","", "");
        await this.ajouteTrait(monActor101,"Circonspect","", "");
        await this.ajouteTrait(monActor102,"Médecine","", "");
        await this.ajouteTrait(monActor102,"Herboristerie","", "");
        await this.ajouteTrait(monActor102,"Miséricordieux","", "");
        await this.ajouteTrait(monActor102,"Patient","", "");
        await this.ajouteTrait(monActor103,"Connaissances régionales","Connaissances régionales [Dale]", "");
        await this.ajouteTrait(monActor103,"Menuisier","", "");
        await this.ajouteTrait(monActor103,"Vue perçante","", "");
        await this.ajouteTrait(monActor103,"Sinistre","", "");
        await this.ajouteTrait(monActor104,"Ménestrel","", "");
        await this.ajouteTrait(monActor104,"Conte","Conteuse", "");
        await this.ajouteTrait(monActor104,"Astucieux","Astucieuse", "");
        await this.ajouteTrait(monActor104,"Beau","Belle", "");
        await this.ajouteTrait(monActor105,"Monte-en-l-air","", "");
        await this.ajouteTrait(monActor105,"Commerce","", "");
        await this.ajouteTrait(monActor105,"Preste","", "");
        await this.ajouteTrait(monActor105,"Cachottier","", "");
        await this.ajouteTrait(monActor106,"Conte","", "");
        await this.ajouteTrait(monActor106,"Majestueux","", "");
        await this.ajouteTrait(monActor106,"Téméraire","", "");
        await this.ajouteTrait(monActor107,"Connaissances régionales","Connaissances régionales [Dale]", "");
        await this.ajouteTrait(monActor107,"Sinistre","", "");
        await this.ajouteTrait(monActor107,"Impétueux","", "");
        await this.ajouteTrait(monActor108,"Connaissances régionales","Connaissances régionales [Dale]", "");
        await this.ajouteTrait(monActor108,"Commerce","", "");
        await this.ajouteTrait(monActor108,"Malin","", "");
        await this.ajouteTrait(monActor108,"Circonspect","", "");
        await this.ajouteTrait(monActor109,"Connaissance des ennemis [race]","Connaissance des ennemis [Dragons]", "");
        await this.ajouteTrait(monActor109,"Herboristerie","", "");
        await this.ajouteTrait(monActor109,"Sévère","", "");
        await this.ajouteTrait(monActor109,"Vengeur","", "");
        await this.ajouteTrait(monActor110,"Connaissances régionales","Connaissances régionales [Montagnes Grises]", "");
        await this.ajouteTrait(monActor110,"Montagnard","", "");
        await this.ajouteTrait(monActor110,"Déterminé","", "");
        await this.ajouteTrait(monActor110,"Endurci","", "");
        await this.ajouteTrait(monActor111,"Déterminé","", "");
        await this.ajouteTrait(monActor111,"Inébranlable","", "");
        await this.ajouteTrait(monActor111,"Circonspect","", "");
        await this.ajouteTrait(monActor111,"Connaissances régionales","Connaissances régionales [Monts de Fer]", "");
        await this.ajouteTrait(monActor111,"Forge","", "");
        await this.ajouteTrait(monActor112,"Batelier","Batelière", "");
        await this.ajouteTrait(monActor112,"Commerce","", "");
        await this.ajouteTrait(monActor112,"Courageux","Courageuse", "");
        await this.ajouteTrait(monActor112,"Jovial","Joviale", "");
        await this.ajouteTrait(monActor113,"Connaissances régionales","Connaissances régionales [Rhovanion]", "");
        await this.ajouteTrait(monActor113,"Commerce","", "");
        await this.ajouteTrait(monActor113,"Ouïe fine","", "");
        await this.ajouteTrait(monActor113,"Cachottier","", "");
        await this.ajouteTrait(monActor114,"Connaissances régionales","Connaissances régionales [Marches de Dale]", "");
        await this.ajouteTrait(monActor114,"Tradition ancienne","", "");
        await this.ajouteTrait(monActor114,"Vue perçante","", "");
        await this.ajouteTrait(monActor114,"Robuste","", "");

                
        



        console.log("===== FIN CREATION DES PNJ ====");

    }

    async ajouteCompetenceNotable(monActor, _nomCompetenceNotable, _rang, _fav) {
        // Chercher le trait dans les Items
        const monItemOrigine = game.items.contents.find(i => i.type === "skill" && i.name === _nomCompetenceNotable);
    
        // Créer une copie de l'item dans la feuille de personnage
        await monActor.createEmbeddedDocuments('Item',[monItemOrigine], { renderSheet: false });
    
        // récupérer l'item dans la feuille de perso
        const monNouvelItem = monActor.items.find(i => i.type === "skill" && i.name === _nomCompetenceNotable);

        // Construire la mise à jour des données
        const monUpdate = {
            _id: monNouvelItem._id,
            data: {
                value: _rang,
                favoured: {
                    value: _fav
                }
            }
        };
    
        // Appliquer la mise à jour
        const monUpdated = await monActor.updateEmbeddedDocuments('Item',[monUpdate]);
    }

    async ajouteTrait(monActor, _nomTrait, _nomTraitRenomme, _maDescription) {
        // Chercher le trait dans les Items
        const monItemOrigine = game.items.contents.find(i => i.type === "trait" && i.name === _nomTrait);
    
        // Créer une copie de l'item dans la feuille de personnage
        await monActor.createEmbeddedDocuments('Item',[monItemOrigine], { renderSheet: false });
    
        // récupérer l'item dans la feuille de perso
        const monNouvelItem = monActor.items.find(i => i.type === "trait" && i.name === _nomTrait);

        // Déterminer le nom final
        let newNom = _nomTrait;
        if (_nomTraitRenomme !== "") { 
            newNom = _nomTraitRenomme;
        }

        // Déterminer la description finale
        let newDescription = "";
        if (_maDescription !== "") {
            // => nouvelle description qui remplace la précédente
            newDescription = "<p>" + _maDescription + "</p>";
        } else {
            // On garde la description originale du Trait
            newDescription = monNouvelItem.system.description.value;
        }

        // Construire la mise à jour des données
        const monUpdate = {
            _id: monNouvelItem._id,
            name: newNom,
            data: {
                description: {
                    value: newDescription
                }
            }
        };
    
        // Appliquer la mise à jour
        const monUpdated = await monActor.updateEmbeddedDocuments('Item',[monUpdate]);
    }

    async ajouteUnPNJ(_dossier, _nom, _img, _attLevel, _endurance, _description, _weary, _tokenLie, _disposition) {
        console.log("==> ajout de " + _nom);
        let monActor = await Actor.create({ name: _nom, type: "npc", folder: _dossier._id, img: _img, data: { stateOfHealth: { weary: { value: false } }, attributeLevel: { value: _attLevel }, endurance: { value: _endurance, max: _endurance }, description: { value: _description } } });

        const update = {
        };
        update.token = {
            actorLink: _tokenLie,
            disposition: _disposition,
            bar1: {
                attribute: "endurance"
            },
            displayBars: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER,
            displayName: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER,
            vision: false
        };
        await monActor.update(update);
        
        return monActor;
    }
}